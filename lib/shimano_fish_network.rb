module ShimanoFishNetwork
  module Validators
    autoload :AbsenceValidator, 'shimano_fish_network/validators/absence_validator.rb'
  end
  autoload :Newsletter, 'shimano_fish_network/newsletter.rb'
  autoload :FakeNewsletter, 'shimano_fish_network/fake_newsletter.rb'
end
