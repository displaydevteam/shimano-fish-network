module ShimanoFishNetwork
  class Newsletter
    API_KEY = 'a5525babb6e5cfb17231ad8342988c82-us4'
    LIST = 'e820fee6c5'
    attr_accessor :proxy, :list

    def initialize
      @proxy = Gibbon.new
      @proxy.api_key = API_KEY
    end

    def subscribe_user(user)
      data = format_user_data(user)
      @proxy.list_subscribe :id => LIST,
                            :email_address => user.email,
                            :merge_vars => data,
                            :update_existing => true,
                            :double_optin => false,
                            :send_welcome => false

      return data
      rescue Gibbon::MailChimpError => exception
        puts 'noway'
    end
    
    def unsubscribe_user(user)
      @proxy.list_unsubscribe :id => LIST,
                              :email_address => user.email,
                              :send_goodbye => false
    end

    private
    def format_user_data(user)
      attributes = Fisher.mailchimp_attributes.select{ |a| a != :email }
      data = {}
      attributes.each_with_index do |attribute, index|
        if attribute == :name || attribute == :surname
          key = 'FNAME' if attribute == :name
          key = 'LNAME' if attribute == :surname
        else
          key = "MMERGE#{index+1}"
        end
        value = user.send(attribute)
        value = value.to_s.upcase if !!value == value
        data[key] = value
      end

      return data
    end
  end
end
