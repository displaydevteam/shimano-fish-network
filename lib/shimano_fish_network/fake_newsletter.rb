module ShimanoFishNetwork
  class FakeNewsletter < Newsletter
    attr_accessor :lists

    def initialize
      @list = {}
    end

    def reset
      @list = {}
    end
    
    def get_user_info(email)
      return format_return_value(@list[email])
    end

    def subscribe_user(user)
      user_data = format_user_data(user)
      @list[user.email] = user_data

      return user_data
    end
    
    def unsubscribe_user(user)
      @list.delete user.email
    end

    private 
    def format_return_value(value)
      success = value.present? ? 1 : 0
      return { 'success' => success, 'data' => value }
    end
  end
end
