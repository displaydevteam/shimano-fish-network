namespace :db do
  namespace :export do

    def export(records, model_name)
      I18n.locale = :it
      model = model_name.underscore.pluralize
      model_class = model_name.classify.constantize
      contents = records.to_xls(:columns => model_class.exportable_attributes, :headers => model_class.exportable_attributes.collect{|a| a.to_s.humanize})
      uploader = ExportUploader.new
      temp_dir = Rails.root.join('tmp')
      Dir.mkdir(temp_dir) unless Dir.exists?(temp_dir)
      file_path = "#{Rails.root}/tmp/#{model}.xls"
      File.open(file_path, 'wb') do |f|
        f.write contents
      end
      file = File.open(file_path)
      uploader.store! file
      file.close
      File.delete(file_path)
    end

    desc "Exports fishers in xls format"
    task :fishers => :environment do
      records = Fisher.includes(:products)
      export records, 'Fisher'
    end

    desc "Exports product_registrations in xls format"
    task :product_registrations => :environment do
      records = ProductRegistration.includes(:fisher, :product).scoped
      export records, 'ProductRegistration'
    end

    desc "Exports fishers in xls format"
    task :stores => :environment do
      records = Store.scoped
      export records, 'Store'
    end
    
    desc "Exports fishers in xls format"
    task :products => :environment do
      records = Product.scoped
      export records, 'Product'
    end
  end
end