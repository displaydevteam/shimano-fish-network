# encoding: UTF-8
require 'csv'
namespace :db do
  
  namespace :import do

    namespace :stores do
      desc "Add stores without deleting"
      task :add, [:file] => :environment do |t,args|
        args.with_defaults(:file => "#{Rails.root}/db/csv/stores.csv")
        CSV.foreach(args[:file], :encoding => 'UTF-8') do |line|
          store = Store.new
          store.name = line[0].try(:strip).try(:titleize)
          store.address = line[1].try(:strip).try(:titleize)
          store.cap = line[2].try(:strip).try(:titleize)
          store.city = line[3].try(:strip).try(:titleize)
          store.province = line[4].try(:strip)
          store.phone = line[5].try(:strip)
          store.region = line[6].try(:strip).try(:titleize)
          store.website = line[-1].try(:strip).try(:downcase)
          if store.save
            puts "salvato store #{store.name}"
          else
            puts "errore salvataggio store #{store.name}"
          end
        end
      end
      
      desc "Destroy all stores"
      task :destroy => :environment do 
        Store.destroy_all
      end
      
      desc "Replace stores deleting all existing stores"
      task :replace, [:file] => :environment do |t,args|
        args.with_defaults(:file => "#{Rails.root}/db/csv/stores.csv")
        Rake::Task["db:import:stores:destroy"].invoke
        Rake::Task["db:import:stores:add"].invoke(args[:file])
      end
      
      desc "Updates stores"
      task :update, [:file] => :environment do |t,args|
        args.with_defaults(:file => "#{Rails.root}/db/csv/stores.csv")
        CSV.foreach(args[:file], :encoding => 'UTF-8') do |line|
          
        end
      end
      
    end  
    
    desc "Destroy all stores"
    task :destroy => :environment do 
      Product.destroy_all
    end
    
    desc "Import products from csv"
    task :products, [:file, :destroy_all] => :environment do |t,args|
      args.with_defaults(:file => "#{Rails.root}/tmp/products.csv")
      header_parsed = false
      CSV.foreach(args[:file], :encoding => 'UTF-8') do |line|
        if header_parsed
          product = Product.new
          product.product_type = line[0].try(:strip).try(:titleize)
          product.family = line[1].try(:strip).try(:titleize)
          product.category = line[2].try(:strip).try(:titleize)
          product.code = line[3].try(:strip).try(:upcase)
          product.description = line[4..-1].uniq.join(" ").try(:strip).try(:upcase)
          if product.save
            puts "salvato prodotto #{product.code}"
          else
            puts "errore salvataggio prodotto #{product.code}"
          end
        else
          header_parsed = true
        end
      end
    end
    
    desc "Import catalogue items"
    task :catalogue_items, [:file] => :environment do |t, args|
      args.with_defaults(:file => "#{Rails.root}/tmp/catalogue_items.csv")
      header_parsed = false
      CSV.foreach(open(args[:file]), :encoding => 'UTF-8') do |line|
        if header_parsed
          product_family = ProductFamily.find_or_initialize_by_url(line[1].strip.try(:parameterize))
          product_family.product_type = ProductType.find_by_name(line[0])
          product_family.save :validate => :false
          if line[2].strip.present?
            tag = Tag.find_or_initialize_by_url(line[2].strip.try(:parameterize))
            tag.name = line[2]
            tag.save
          end
          data = {
            :name => line[3],
            :url => line[3].parameterize,
            :description => line[4],
            :sizes => line[5],
            :product_family_id => product_family.id
          }
          item = CatalogueItem.new(data)
          item.tags << tag if line[2].strip.present?
          item.save :validate => false
          puts "creato #{data[:name]}"
        else
          header_parsed = true
        end
      end
    end

  end

end

namespace :product_finder do
  desc 'Imports questions and answers'
  task :import => :environment do
    ProductFinderQuestion.destroy_all
    ProductFinderAnswer.destroy_all
    ProductFinderProduct.destroy_all
    ProductFinderProductanswer.destroy_all
    CSV.foreach('db/csv/product-finder.csv', :col_sep => ';') do |row|
      root_question = ProductFinderQuestion.find_or_create_by_title(row[0])
      answer_to_first_question = ProductFinderAnswer.find_or_create_by_product_finder_question_id_and_title(root_question.id, row[1])
      second_question = ProductFinderQuestion.find_or_create_by_product_finder_answer_id_and_title answer_to_first_question.id, row[2]
      answer_to_second_question = ProductFinderAnswer.find_or_create_by_product_finder_question_id_and_title(second_question.id, row[3])

      product = ProductFinderProduct.find_or_create_by_product_type_and_fishing_reel_name_and_fishing_rod_name(row[6], row[7], row[10])
      product.fishing_reel_link = row[8]
      product.fishing_reel_image = row[9]
      product.fishing_rod_link = row[11]
      product.fishing_rod_image = row[12]
      product.save

      if row[4] != '-'
        third_question = ProductFinderQuestion.find_or_create_by_product_finder_answer_id_and_title answer_to_second_question.id, row[4]
        if row[5] != '-'
          answer_to_third_question = ProductFinderAnswer.find_or_create_by_product_finder_question_id_and_title(third_question.id, row[5])
          answer_to_third_question.product_finder_products << product
        end
      else
        answer_to_second_question.product_finder_products << product
      end
    end
  end
end
