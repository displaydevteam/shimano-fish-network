// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-ui
//= require jquery_ujs
//= require select2
//= require cocoon
//= require fit.js
//= require popup
//= require masonry

$(document).ready(function(){

console.log('loaded')
var store_locator = document.querySelector('.reserve_block_map')
if ( store_locator ) {
  var markers_props = store_locator.getAttribute('data-markers')
  var markers = JSON.parse(markers_props)
  console.log(markers_props)

  geocoder = new google.maps.Geocoder();

  $('.js-reserve-next').on("click",function(e){
    $('.reserve_block_cont').removeClass('active')
    $('.reserve_block_cont:eq( 1 )').addClass('active')
    $('.reserve_block_action').removeClass('active')
    $('.reserve_block_action:eq( 1 )').addClass('active')
    initMap();
  })
  

  function initMap() {
    var myLatLng = {lat: -25.363, lng: 131.044};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: myLatLng
    });


    var bounds = new google.maps.LatLngBounds();
    $(markers).each(function( index ) {
      codeAddress(markers[index].lat_lng,map,bounds,index)
    });

  }

  function codeAddress(address,map,bounds,index) {

        var marker = new google.maps.Marker({
            map: map,
            position: address
        });
        marker.addListener('click', function() {
          $('.reserve_block_cont').removeClass('active')
          $('.reserve_block_cont').last().addClass('active')
          $('.reserve_block_action').removeClass('active')
          $('.reserve_block_action').last().addClass('active')
          $('.reserve_block_store_detail').removeClass('active')
          $('.reserve_block_store_detail').eq(index).addClass('active')

        });
        bounds.extend(address);
        map.fitBounds(bounds);
  }

}

$("#e1").select2({
    placeholder: "Cerca il tuo mulinello",
    width: '100%'
});
$(".special_s2").select2({
    placeholder: "Cerca il prodotto"
});

$('.js-closepop').click(function(e){
  e.preventDefault();
  $('#cookiebot').removeClass('open')
})


$('#dealer_assistance_request_product_id').select2({
  ajax: {
    url: "/products_dealers.json",
    cache: true
  }
});
$("#e2").select2({
    placeholder: "Cerca il tuo negozio",
    width: '100%'
});
 $('.dealers').on('cocoon:after-insert', function(e, insertedItem) {
    $(insertedItem).find(".special").select2({
      placeholder: "Cerca il prodotto"
  });
  });
var TABLET_BREAKPOINT = 1024;


$('#e1').change(function(event) {
  val = 'https://s3-eu-west-1.amazonaws.com/shmnassets/manuals/' + $(this).val();
  $('#downbtn').attr("href",val)
  $('#downbtn').removeClass("disabled");
});
$('#e2').change(function(event) {
  val = window.location.href + '/attend/' + $(this).val();
  $('.js-attend').attr("href",val)
  $('.js-attend').removeClass("disabled");
});

function fixImages(images) {
    var width = $(window).innerWidth(),
        attr = 'data-desktop',
        img, path;

    if(images === undefined || images === null) {
      images = document.images;
    }


    if (width < TABLET_BREAKPOINT) {
      attr = 'data-mobile';
    }

    for(var i = 0, len = images.length; i < len; i++) {
      img = images[i];
      path = img.getAttribute(attr);

      if (path) {
        img.src = path;
      }
    }
  }

fixImages();

$( window ).resize(function() {
  fixImages();
});

$('#blog').fitVids();
$(".js-modal").leanModal();

    $(".js-menu").click(function(ev) {
    	ev.preventDefault();
        $('.js-menu-target').toggleClass('open')
    });

    $('.js-close-con').click(function(ev) {
      ev.preventDefault(ev);
      $(this).parent().fadeOut( "slow", function() {
          $('.hero').addClass("js-conready");
           $('.js-conready').click(function(ev) {
          $('.overeffect').fadeIn( "slow", function() {
            window.location.href = "/concorso/gioca/risultato";
          });
        })
      });
    

    })

$('#store').find('select').change(function() {
     $(this).closest("form").submit();
   });

$(window).load(function(ev){
var $container = $('#mason');
// init
$container.masonry({
  itemSelector: '.item',
  gutter: 0,
});
})


$(window).scroll(function(){
 hheight = $('#htrigger').offset().top;
 wscroll = $(window).scrollTop();
 if ( wscroll > hheight) {
  $('body').addClass('headfix');
  $('#htrigger').css("padding-top",115)
 } else {
  $('body').removeClass('headfix');
  $('#htrigger').css("padding-top",0)
 }
 });

});
