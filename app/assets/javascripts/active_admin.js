//= require active_admin/base
//= require token
//= require ckeditor/init
//= require active_admin/select2
//= require best_in_place
//= require best_in_place.purr
//= require autocomplete-rails
//= require s3_direct_upload
/* Active Admin JS */

$(function() {
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#s3-uploader").S3Uploader();
   $("#s3-uploader").bind("s3_upload_complete", function(e, content) {
            attachment = content.url;
            $('#dealer_attachment_file').val(attachment)
   });
  

    $('.best_in_place').best_in_place()


    $(".clear_filters_btn").click(function() {
        window.location.search = "";
        return false;
    });


    $('#catalogue_item_tag_tokens').tokenInput('/tags.json', {
        theme: 'facebook',
        prePopulate: $('#catalogue_item_tag_tokens').data('load')
    });

    $('#post_tag_tokens').tokenInput('/posttags.json', {
        theme: 'facebook',
        prePopulate: $('#post_tag_tokens').data('load')
    });

    $('form').on('click', '.remove_item', function(event) {
        $(this).parent().prev().find('input[type=hidden]').val('1');
        $(this).closest('fieldset').hide()
        event.preventDefault();
    });

    $('form').on('click', '.add_fields', function(event) {
        time = new Date().getTime();
        regexp = new RegExp($(this).data('id'), 'g');
        $(this).before($(this).data('fields').replace(regexp, time));
        event.preventDefault();
    });

});