ActiveAdmin.register ProductFinderAnswer do
	
  menu false
  index do
  	column :title
  	column "Question" do |a|
  		ProductFinderQuestion.find_by_id(a.product_finder_question_id).title
  	end
  	actions
  end

end