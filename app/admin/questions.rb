ActiveAdmin.register Question do
  menu parent: "Questions"
  filter :title
  filter :question
  index do
    column :fisher do |q|
      q.fisher_complete_name
    end
    column :title
    column :question
    column :published do |q|
      status_tag q.published? ? 'published' : 'draft'
    end
    column :created_at
    column :position
    actions
  end

  show do
    attributes_table do
      row :fisher do |q|
        q.fisher.complete_name
      end
      row :title
      row :question
      row :published do |q|
        status_tag q.published? ? 'published' : 'draft'
      end
      row :created_at
      row :position
      row :answers do |q|
        q.answers.count
      end
    end
  end

  form do |f|
    f.inputs do "Details"
      f.input :title
      f.input :question 
      f.input :position
    end
    f.actions
  end 

  action_item :only => [:show, :edit] do
    link_to "Answers", administration_question_answers_path(question)
  end

end
