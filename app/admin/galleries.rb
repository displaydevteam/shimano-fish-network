ActiveAdmin.register Gallery do
  menu false


  index do
    column :id
    column :title
    column :description do |gallery|
      gallery.description.truncate 200
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :description
      row :created_at
      row :updated_at
    end
   
  end

end

