ActiveAdmin.register ProductRegistration, as: "Registration" do
  filter :created_at
  filter :fisher_email, as: :string

  index do
    column :id
    column :store do |product_registration|
      product_registration.store.name if product_registration.store.present?
    end
    column :product do |product_registration|
      product_registration.product.code if product_registration.product.present?
    end
    column :fisher do |f|
      f.fisher.complete_name
    end
    column :address
    column :purchase_date
    column :warranty_expiration_date
    column :product_photo do |p|
      if p.product_photo.present?
        link_to(image_tag(p.product_photo.url(:thumb)), p.product_photo.url)
      end
    end
    column :receipt_photo do |p|
      if p.receipt_photo.present?
        link_to(image_tag(p.receipt_photo.url(:thumb)), p.receipt_photo.url)
      end
    end
    column :order_value
    actions
  end

  form do |f|
    f.inputs "details" do
      f.input :fisher_id, input_html: { value: (params[:fisher_id].present? ? params[:fisher_id] : "") }
      f.input :product, as: :select2
      f.input :store, as: :select2
      f.input :purchase_date, :as => :datepicker
      f.input :warranty_expiration_date, :as => :datepicker
    end
    f.actions
  end

  show do |pr|
    attributes_table do
      row :fisher do
        pr.fisher.email
      end
      row :product
      row :store
      row :purchased_at
      row :warranty_expiration_date
      row :assistence_requests do
        pr.assistence_requests.count
      end
    end
  end
end
