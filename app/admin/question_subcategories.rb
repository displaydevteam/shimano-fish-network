ActiveAdmin.register QuestionSubcategory do
	menu parent: "Questions"
  config.filters = false
  
  index do
    column :id
    column :name
    column :published do |qs|
      status_tag qs.published? ? 'published' : 'draft'
    end
    actions
  end


end
