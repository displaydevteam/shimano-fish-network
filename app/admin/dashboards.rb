ActiveAdmin.register_page "Dashboard" do

  content do
    h4 do
      "Statistics"
    end
    ul do
      li "Total users: #{Fisher.count}"
      li "Users with registered products: #{ProductRegistration.pluck(:fisher_id).uniq.count}"
      li "Total registered products: #{ProductRegistration.count}"
     end
    
    h4 do
      "Downloads - Updated at 6am"
    end
    ul do
      li link_to("Product Registrations", "https://shmnassets.s3.amazonaws.com/exports/product_registrations.xls")
      li link_to("Fishers", "https://shmnassets.s3.amazonaws.com/exports/fishers.xls")
      li link_to("Stores", "https://shmnassets.s3.amazonaws.com/exports/stores.xls")
      li link_to("Products", "https://shmnassets.s3.amazonaws.com/exports/products.xls")
    end

     ul do
      Answer.published.order('created_at DESC').limit(30).each do |answer|
        li do
          raw [ (content_tag :p, answer.text, :style => "width: 600px"),
                (content_tag :p, (link_to "Visualizza", question_path(:category_url => answer.question.question_subcategory.question_category.url, :subcategory_url => answer.question.question_subcategory.url , :id => answer.question), html_options = {:target => 'blank'})),
                (content_tag :p, (link_to "Modera", edit_administration_question_answer_path(answer.question, answer)))].join
        end

      end
    end
  end



  # Define your dashboard sections here. Each block will be
  # rendered on the dashboard in the context of the view. So just
  # return the content which you would like to display.
  
  # == Simple Dashboard Section
  # Here is an example of a simple dashboard section
  #
  #   section "Recent Posts" do
  #     ul do
  #       Post.recent(5).collect do |post|
  #         li link_to(post.title, admin_post_path(post))
  #       end
  #     end
  #   end
  
  # == Render Partial Section
  # The block is rendered within the context of the view, so you can
  # easily render a partial rather than build content in ruby.
  #
  #   section "Recent Posts" do
  #     div do
  #       render 'recent_posts' # => this will render /app/views/admin/dashboard/_recent_posts.html.erb
  #     end
  #   end
  
  # == Section Ordering
  # The dashboard sections are ordered by a given priority from top left to
  # bottom right. The default priority is 10. By giving a section numerically lower
  # priority it will be sorted higher. For example:
  #
  #   section "Recent Posts", :priority => 10
  #   section "Recent User", :priority => 1
  #
  # Will render the "Recent Users" then the "Recent Posts" sections on the dashboard.
  
  # == Conditionally Display
  # Provide a method name or Proc object to conditionally render a section at run time.
  #
  # section "Membership Summary", :if => :memberships_enabled?
  # section "Membership Summary", :if => Proc.new { current_admin_user.account.memberships.any? }

end