ActiveAdmin.register QuestionCategory do
  menu parent: "Questions"
  config.filters = false
  
  index do
    column :id
    column :name
    column :description
    column :position
    column :group
    actions
  end

  show do |qc|
    attributes_table do
      row :id
      row :name
      row :description
      row :position
      row :group
      row :subcategories do
        qc.question_subcategories.count
      end
    end
  end


end
