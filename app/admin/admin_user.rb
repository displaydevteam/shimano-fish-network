ActiveAdmin.register AdminUser do

  index do
   column :id
   column :email
   column :role
   actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :email
      f.input :password
      f.input :role, :as => :select, :collection => %w(admin assistancer moderator)
    end
    f.actions
  end

  controller do
    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end
  end
end
