ActiveAdmin.register DealerAssistanceRequest do
  
  menu false

  index do
    column :sku
    column :status
    column :dealer
    column :date
    actions
  end

  form do |f|
  	f.inputs "Details" do
  		f.input :dealer, as: :select_2
  		f.input :sku, :input_html => { :disabled => true } 
  		f.input :date, as: :datepicker
      f.input :status, as: :select, collection: DealerAssistanceRequest.statuses
  	end
    f.inputs "Requests" do
      f.has_many :dealer_assisteance_request_products do |b|
        if b.object.product
        b.input :product_id, as: :select_2, collection: Product.where(:id => b.object.product.id ), :input_html => { "data-ajax--url" => "/product_list.json" }
        else
        b.input :product_id, as: :select_2, collection: Product.first(5), :input_html => { "data-ajax--url" => "/product_list.json" }
        end 
        b.input :warranty
        b.input :receipt
        b.input :receipt_date, as: :datepicker
        b.input :client_full_name
        b.input :client_email
        b.input :problem_description
        b.input :request_part
      end
    end
  	f.actions
  end 		
end
