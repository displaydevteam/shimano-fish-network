ActiveAdmin.register Event do
  menu parent: "Events"
  filter :title
  filter :start_date

  index do
  	column :title
  	column :description
  	column :start_date
  	column :end_date
  	actions
  end
end
