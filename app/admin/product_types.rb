ActiveAdmin.register ProductType do
	
  menu false
  action_item :only => [:edit, :show] do
    link_to 'Product Families', administration_product_type_product_families_path(product_type)
  end

end
