ActiveAdmin.register CatalogueItem do
  form :partial => 'form'
  menu false

  controller do
    before_filter :build_resource, :only => [:new]
    before_filter :build_stuff, :only => [:new, :edit]

    private
    def build_stuff
      resource.product_images.build
    end
  end


end
