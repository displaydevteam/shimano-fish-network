ActiveAdmin.register Document do
  form do |f|
  	f.inputs "Campi" do
  		f.input :title
  		f.input :sku
  		f.input :doc_type, :collection => ['esploso','manuale']
  		f.input :attachment
  	end
  	f.actions
  end
end
