ActiveAdmin.register UserEvent do
   menu parent: "Events"
  filter :event
  index do 
  	column :code
  	column "User" do |event|
    	event.fisher.complete_name
	end
	column "Event" do |user|
    	user.event.title if user.event.present?
	end
  	column :created_at
  	actions

  end
end
