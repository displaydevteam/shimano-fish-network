ActiveAdmin.register Post do
  config.sort_order = 'date_desc'
  menu parent: "Blog"

  filter :post_category
  filter :title
  
  index do
  	column :title
    column :image do |post| 
      image_tag(post.image.url(:thumb))
    end
    column :date
    column "Permalink" do |post|
      link_to "/blog/#{post.date}/#{post.title.parameterize}", "/blog/#{post.date}/#{post.title.parameterize}", :target => "_blank"
    end
  	actions
  end

  form do |f|
    f.inputs do "Details"
      f.input :post_category
      f.input :highlight
      f.input :date, :as => :datepicker
      f.input :title
      f.input :image
      f.input :remove_image, :as => :boolean
      f.input :description, :as => :ckeditor
      f.input :fisher_content, :as => :ckeditor
      f.input :published
      f.input :tag_tokens, :input_html => { :data => { :load =>  resource.post_tags } } 
    end
    f.actions
  end
end
