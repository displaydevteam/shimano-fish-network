ActiveAdmin.register PrizeTicket do

	index do 
		column :email
		column :registration do |pt|
			raw Fisher.find_by_email(pt.email).product_registrations.where("order_value > ?",0).map{|pr| "#{pr.product.code}, #{pr.store.name}, #{pr.order_value}"}.join("<br>")
		end
		actions
	end	

end
