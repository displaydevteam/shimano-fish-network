ActiveAdmin.register ProductFinderProduct do

  menu false
index do
	column :product_type
	column :fishing_reel_name
	column :fishing_rod_name
	column :ordering do |pos|
		best_in_place pos, :ordering, :path => [:administration,pos]
	end
	actions
end

form do |f|

 f.inputs "Dettagli" do
 	f.input :ordering
 	f.input :product_type
 	f.input :fishing_reel_name
 	f.input :fishing_reel_link
 	f.input :fishing_reel_image
 	f.input :fishing_rod_name
 	f.input :fishing_rod_link
 	f.input :fishing_rod_image
 end
 f.inputs "Prodotti" do
 	f.input :product_finder_answers, :as => :select2_multiple
 end
 f.actions
end

end