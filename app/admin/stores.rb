ActiveAdmin.register Store do
	filter :name
	filter :cap

	index do
		column :name
		column :region
		column :city
		column :tech
		column :x_store
		column :reserve
		actions
	end

	form do |f|
		f.inputs "Details" do 
			f.input :reserve
			f.input :name
			f.input :region
			f.input :province
			f.input :city
			f.input :address
			f.input :cap
			f.input :phone
			f.input :website
			f.input :email
			f.input :hidden
			f.input :x_store
			f.input :tech
		end
		f.actions
	end
end
