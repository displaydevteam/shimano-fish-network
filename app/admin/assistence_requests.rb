ActiveAdmin.register AssistenceRequest do
  belongs_to :product_registration
  config.clear_sidebar_sections!

  index do
    column :product_registration
    column :started_at
    column :finished_at
    column :notes
    default_actions
  end

  show do |ar|
    attributes_table do 
      row :product_registration
      row :started_at
      row :finished_at
      row :notes
    end
  end

  form :partial => 'form'
end
