ActiveAdmin.register HomepageContent do

  index do
   column :id
   column :title
   column :abstract
   column :link
   column :video
   column :published do |homepage_content|
     status_tag homepage_content.published? ? 'published' : 'draft'
   end
   actions
 end

  show do
   attributes_table do
     row :id
     row :title
     row :abstract
     row :link
     row :video
     row :new_window
     row :published do |homepage_content|
       status_tag homepage_content.published? ? 'published' : 'draft'
     end
     row :highlight
     row :position
   end
 end

end
