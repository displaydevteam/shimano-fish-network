ActiveAdmin.register Fisher do
  form :partial => "form"
  filter :surname
  filter :email
  filter :campaign_1

  index do
    column :id
    column :surname_and_name, :sortable => :surname do |fisher|
      "#{fisher.surname} #{fisher.name}"
    end
    column :add_registration do |f|
      link_to "Nuova registrazione", "https://www.shimanofishnetwork.it/administration/registrations/new?fisher_id=#{f.id}"
    end
    column :view_registration do |f|
      #francesco.petrella@live.com
      url = URI.encode("https://www.shimanofishnetwork.it/administration/registrations?q[fisher_email_contains]=#{f.email}")
      link_to "Vedi registrazioni", url
    end
    column :email
    actions
  end

  show do |fisher|
    attributes_table do
      row :id
      row :complete_name
      row :email
      row :year_of_birth
      row :region
      row :job
      row :agonist do
        fisher.agonist? ? "Yes" : "No"
      end
      row :sea_techniques do
        fisher.sea_techniques.select { |t| t.present? }.join(", ")
      end
      row :inner_waters_techniques do
        fisher.inner_waters_techniques.select { |t| t.present? }.join(", ")
      end
      row :product_registrations do
        fisher.product_registrations.count
      end
    end
  end
end
