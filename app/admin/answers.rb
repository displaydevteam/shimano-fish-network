ActiveAdmin.register Answer do
  belongs_to :question, parent_class: Question 

  filter :text
  index do
    column :id
    column :text
    column :published do |a|
      status_tag a.published? ? 'published' : 'draft'
    end
    column :created_at
    column :likes
    actions
  end

  show do
    attributes_table do
      row :id
      row :text
      row :published do |a|
        status_tag a.published? ? 'published' : 'draft'
      end
      row :created_at
      row :likes
    end
  end

  form do |f|
    f.inputs do "Details"
      f.input :text
      f.input :published
    end
    f.actions
  end 

end
