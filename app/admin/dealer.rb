ActiveAdmin.register Dealer do
  menu parent: "Dealers"
  index do
   column :id
   column :chatbot
   column :email
   actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :email
      f.input :password
      f.input :store, :as => :select_2
      f.input :dealer_code
      f.input :chatbot
    end
    f.actions
  end

  controller do
    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end
  end
end
