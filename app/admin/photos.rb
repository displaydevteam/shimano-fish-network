ActiveAdmin.register Photo do
  
  menu false
  form :partial => 'form'

  filter :published

  index do
    column :id
    column :user do |photo|
      if photo.fisher
      photo.fisher.name
      end
     end
    column :image do |photo|
      if photo.image.url(:thumb)
       image_tag photo.image.url(:thumb)
      end
    end
    column :description
    column :published do |photo|
      status_tag photo.published? ? 'published' : 'draft'
    end
    actions
  end

  member_action :publish, :method => :put do
    if update_resource(resource, [:published => true] )
      redirect_to administration_gallery_path(resource.gallery), :notice => "Published!"
    else
      redirect_to administration_gallery_path(resource.gallery), :alert => 'Something wrong happend'
    end
  end

  controller do
    
    def destroy
      CommunityPhotoMailer.photo_refused(resource).deliver
      super
    end

    private
    def update_resource(object, attributes)
      object.assign_attributes *attributes, :as => :admin
      if object.changes['published'] == [false, true]
        CommunityPhotoMailer.photo_approved(object).deliver
      end
      object.save
    end

  end

end
