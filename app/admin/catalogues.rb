ActiveAdmin.register Catalogue do
 form do |f|
  f.inputs "Details" do
    f.input :title
    f.input :position
    f.input :image
    f.input :link
  end
   f.inputs do
      f.has_many :attachments, :multipart => true, allow_destroy: true, new_record: true do |a|
        a.input :title
        a.input :link
        a.input :url, :as => :file 
        a.input :_destroy, :as=>:boolean, :required => false, :label=>'Remove'
        a.actions
      end
end
  f.actions
  end  
end


