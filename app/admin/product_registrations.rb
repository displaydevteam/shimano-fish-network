ActiveAdmin.register ProductRegistration do
  filter :created_at
  filter :fisher_id

  belongs_to :fisher

  index do
    column :id
    column :product do |product_registration|
      product_registration.product.code if product_registration.product.present?
    end
    column :fisher do |f|
      f.fisher.complete_name
    end
    column :address
    column :purchase_date
    column :warranty_expiration_date
    column :updated_at
    column :product_photo do |p|
      if p.product_photo.present?
        link_to(image_tag(p.product_photo.url(:thumb)), p.product_photo.url)
      end
    end
    column :receipt_photo do |p|
      if p.receipt_photo.present?
        link_to(image_tag(p.receipt_photo.url(:thumb)), p.receipt_photo.url)
      end
    end
    column :order_value
    actions
  end

  form do |f|
    f.inputs "details" do
      f.input :fisher_id, as: :string, :input_html => { :hidden => true }
      f.input :product, as: :select2
      f.input :store, as: :select2
      f.input :purchase_date, :as => :datepicker
      f.input :warranty_expiration_date, :as => :datepicker
    end
    f.actions
  end

  show do |pr|
    attributes_table do
      row :fisher do
        pr.fisher.email
      end
      row :product
      row :store
      row :purchased_at
      row :warranty_expiration_date
      row :assistence_requests do
        pr.assistence_requests.count
      end
    end
  end
end
