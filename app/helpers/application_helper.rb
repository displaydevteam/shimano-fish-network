module ApplicationHelper
  
  def tag_link(tag, product_type, product_family)
    current_tags = (params[:tags] || '').split('/')
    if current_tags.include? tag.url
      current_tags.delete(tag.url)
      current_tags = nil if current_tags.empty?
      if @product_family.present?
        link = catalogue_items_path(:product_type => @product_type.url, :product_family => @product_family.url, :tags => current_tags)
      else
        link = product_families_path(:product_type => @product_type.url, :tags => current_tags)
      end
      return (link_to tag.name, link, :class => 'tag sans')
    else
      new_tags = (current_tags << tag.url).uniq.join('/')
      if @product_family.present?
        return (link_to tag.name, catalogue_items_path(:product_type => @product_type.url, :product_family => @product_family.url, :tags => new_tags), :class => 'label tag sans')
      else
        return (link_to tag.name, product_families_path(:product_type => @product_type.url, :tags => new_tags), :class => 'label tag sans')
      end
    end
  end

  def navigation_link_to(text, url, options = {})
    if request.url =~ /#{url}/ and (url != root_path)
      options[:class] = [options[:class], 'active'].compact.join(' ')
    end
    return link_to(text, url, options)
  end
  
  def youtube_embed(youtube_url)
  if youtube_url[/youtu\.be\/([^\?]*)/]
    youtube_id = $1
  else
    # Regex from # http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url/4811367#4811367
    youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
    youtube_id = $5
  end

  %Q{<iframe width="1280" height="720" src="https://www.youtube.com/embed/5qytHwg7duM?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>}
end

  def render_store(store)
    
    content_tag :ul, :class => "store_fields" do
	  concat(content_tag :li, store.name, :class => "store_name sans") if store.name.present?
      [:address, :half_address, :phone].each do |field|
        content = store.send(field) 
        concat(content_tag :li, content, :class => "store_field") if content.present?
      end
      concat(content_tag  :li, 
                          link_to(  "Vai al sito", 
                                    store.website, 
                                    :target => '_blank',:class => "btn storelink sans")
                                    ) if store.website.present?
#  concat(content_tag :li, 
#                      link_to(  "Visualizza mappa", 
#	                                "http://maps.google.com?q=#{store.gmaps_address}", 
#                                :target => '_blank'), 
#	                      :class => "store_map") if store.complete_address.present?
    end
    
  end
  
end
