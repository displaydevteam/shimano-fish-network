module Admin::AdminHelper
  def admin_navigation_link_to(text, url, options = {})
    parent_class = ''
    if request.url =~ /#{url}/ and (url != root_path)
      options[:class] = [options[:class], 'active'].compact.join(' ')
      parent_class = 'active'
    end
    return content_tag :li, link_to(text, url, options), :class => parent_class
  end
  
end