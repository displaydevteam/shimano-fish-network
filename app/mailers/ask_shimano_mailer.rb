# encoding: UTF-8
class AskShimanoMailer < ActionMailer::Base
  default from: 'info@shimanofishnetwork.it'
  default from: "Shimano community <info@shimanofishnetwork.it>"

  def new_answer(answer)
    @answer = answer
    @question = answer.question
    mail(:to => @question.fisher.email, :subject => 'Nuova risposta alla tua domanda!')
  end

  def new_answerexpert(answer)
    @answer = answer
    @question = answer.question
    expert_email = @question.question_subcategory.email
    mail(:to => expert_email, :subject => 'Nuova risposta su Shimano Fish Network')
  end
  
  def new_question(question)
    @question = question
    expert_email = question.question_subcategory.email
    mail(:to => expert_email, :subject => 'Nuova domanda per te su www.shimanofishnetwork.it')
  end

end
