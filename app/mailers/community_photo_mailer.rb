# encoding: UTF-8
class CommunityPhotoMailer < ActionMailer::Base
  default from: 'info@shimanofishnetwork.it'
  ADMIN_EMAIL = Rails.env.production? ? 'info@shimanofishnetwork.it' : 'some-unexisting-email@email.com'

  def photo_approved(photo)
    @photo = photo
    mail(:to => @photo.fisher.email, :subject => 'La tua foto è stata approvata!')
  end

  def photo_refused(photo)
    @photo = photo
    mail(:to => @photo.fisher.email, :subject => 'La tua foto purtroppo è stata rifiutata')
  end

  def admin_notice(photo)
    @photo = photo
    mail(:to => ADMIN_EMAIL, :subject => "E' stata caricata una nuova foto")
  end
end
