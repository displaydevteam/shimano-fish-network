class EventAttend < ActionMailer::Base
  default from: "info@shimanofishnetwork.it"
  def attend(event)
    @fisher = Fisher.find(event.fisher_id)
    @event =  Event.find(event.event_id)
    @store =  Store.find(event.store_id)
    @code = event.code
    mail(:to => @fisher.email, :subject => "Conferma di partecipazione all'evento Shimano - Pescare Show 2016")
  end 
end
