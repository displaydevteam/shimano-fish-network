class ProductRegistrationMailer < ActionMailer::Base
  default from: "info@shimanofishnetwork.it"
  
  def successful_registration(product_registration)
    @product_registration = product_registration
    mail(:to => @product_registration.fisher.email, :subject => "Registrazione del prodotto avvenuta con successo!")
  end

  def campaign(product_registration)
  	@product_registration = product_registration
    mail(:to => @product_registration.fisher.email, :subject => "Registrazione completata con successo ")
  end

  def campaign_2(product_registration)
    @product_registration = product_registration
    mail(:to => @product_registration.fisher.email, :subject => "Registrazione completata con successo ")
  end

  def assistance(mail,dealer)
  	@dealer = dealer
    mail(:to => mail, :subject => "Nuova richiesta di assistenza #{@dealer.sku}")
  end
end
