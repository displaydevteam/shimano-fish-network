class CommunitiesController < ApplicationController
  def index
  	@questions = QuestionCategory.all
    @last_gallery = Gallery.order('created_at DESC').limit(1).first
    @last_event = Event.order('start_date').last
    @page_title = "Shimano Community"
    @meta_description = "Benvenuto nella Community di Shimano, pensata per tutti i pescatori appassionati che come te, vivono la pesca come un'esperienza da condividere."
  end
end
