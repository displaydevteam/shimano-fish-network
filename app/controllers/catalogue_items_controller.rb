#encoding: UTF-8
class CatalogueItemsController < ApplicationController
  def product_types
    @product_types = ProductType.order("position")
    @page_title = "Attrezzatura per la pesca"
    @meta_description = "Tutto il necessario per creare o completare la tua attrezzatura da pesca. Scopri le canne da pesca i mulinelli, le esche e la buffetteria Shimano"
  end

  def product_families
    @product_types = ProductType.scoped
    @product_type = ProductType.find_by_url(params[:product_type])
    @page_title = @product_type.name
    @meta_description = @product_type.description
    @product_families = @product_type.product_families
    @products = CatalogueItem.published.where(:product_family_id => @product_families.pluck(:id))
    @products = @products.search(params[:q]) if params[:q].present?
    if params[:tags].present?
      @current_tag_ids = Tag.where(:url => params[:tags].split('/')).pluck(:id)
    end
    @tags = Tag.joins(:taggings).where(:taggings => { :catalogue_item_id => CatalogueItem.where(:product_family_id => ProductFamily.where(:product_type_id => @product_type.id)).pluck(:id) }).uniq
    if @current_tag_ids.present?
      catalogue_item_ids = Tagging.select(:catalogue_item_id).where(:tag_id => @current_tag_ids).group(:catalogue_item_id).having('count(catalogue_item_id) = ?', @current_tag_ids.count)
      @products = @products.where(:id => catalogue_item_ids)
    end
    @products = @products.order("name ASC").paginate(:per_page => 12, :page => params[:page])
    render :index
  end

  def index
    @product_types = ProductType.scoped
    @product_type = ProductType.find_by_url(params[:product_type])
    @product_family = @product_type.product_families.find_by_url(params[:product_family])
    @page_title = @product_type.name+' - '+@product_family.name
    @meta_description = "Scopri "+@product_family.name+" di Shimano e trova il modello più adatto al tuo stile!"
    @tags = Tag.joins(:taggings).where(:taggings => { :catalogue_item_id => @product_family.catalogue_items.pluck(:id) }).uniq
    if params[:tags].present?
      @current_tag_ids = Tag.where(:url => params[:tags].split('/')).pluck(:id)
      @tag_for_title  = params[:tags].gsub("-", " ")
      @tag_for_title  = @tag_for_title.gsub("/", " e ")
      @page_title = @product_family.name+" Shimano "+ @tag_for_title
    end
    @products = @product_family.catalogue_items.published
    @products = @products.search(params[:q]) if params[:q].present?
    if @current_tag_ids.present?
      catalogue_item_ids = Tagging.select(:catalogue_item_id).where(:tag_id => @current_tag_ids).group(:catalogue_item_id).having('count(catalogue_item_id) = ?', @current_tag_ids.count)
      @products = @products.where(:id => catalogue_item_ids)
    end
    @products = @products.order("name ASC").paginate(:per_page => 12, :page => params[:page])
  end

  def show

    @product_type = ProductType.find_by_url(params[:product_type])
    @product = CatalogueItem.find_by_url(params[:product_url])
    @product_family = @product.product_family
    @product_types = ProductType.scoped
    @page_title = @product_type.name+' '+@product.name
    @meta_description = "Tutti i dettagli sulla canna da pesca "+@product.name+'. '+@product.description.squish
  end
end
