class CatalogController < ApplicationController
  http_basic_authenticate_with name: "shimano1234", password: "upload1234"
  def index
    @data_manipulation = DataManipulation.new()
  end

  def create
    @data_manipulation = DataManipulation.new(params[:data_manipulation])
    if @data_manipulation.process
      redirect_to root_path, :notice => 'Elaborazione completata con successo!'
    else
      render :action => :index
    end
  end
end
