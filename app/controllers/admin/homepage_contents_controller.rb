class Admin::HomepageContentsController < Admin::AdminController
  inherit_resources
  def update
    update!(:notice => "Conenuto aggiornato con successo!"){ admin_homepage_contents_path }
  end
  def create
    create!(:notice => "Conenuto creato con successo!"){ admin_homepage_contents_path}
  end
  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @homepage_contents ||= @q.result(:distinct => true).paginate(:page => params[:page])
    end
end
