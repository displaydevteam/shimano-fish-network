class Admin::FishersController < Admin::AdminController
  inherit_resources
  
  def update
    update!(:notice => "Dude! Nice job updating that fisher.")
  end
  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @fishers ||= @q.result(:distinct => true).paginate(:page => params[:page])
    end
end
