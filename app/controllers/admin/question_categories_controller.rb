class Admin::QuestionCategoriesController < Admin::AdminController
  inherit_resources

  def create
    create!(:notice => 'Categoria di domande creata con successo!') do
      admin_question_categories_path
    end
  end

  def update
    update!(:notice => 'Categoria di domande aggiornata con successo!') do
      admin_question_categories_path
    end
  end

  def destroy
    destroy!(:notice => 'Categoria di domande rimossa con successo!') do
      admin_question_categories_path
    end
  end

  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @question_categories ||= end_of_association_chain.paginate(:page => params[:page])
    end
end
