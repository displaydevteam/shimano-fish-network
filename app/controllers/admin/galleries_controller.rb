class Admin::GalleriesController < Admin::AdminController
  inherit_resources

  def create
    create!(:notice => 'Galleria creata con successo!'){ admin_galleries_path }
  end

  protected
  def collection
    @q = end_of_association_chain.search(params[:q])
    @galleries ||= @q.result(:distinct => true).paginate(:page => params[:page])
  end
end
