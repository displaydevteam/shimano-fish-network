class Admin::ProductRegistrationsController < Admin::AdminController
  inherit_resources
  belongs_to :fisher
  def destroy
    destroy!(:notice => "Registrazione prodotto eliminata con successo!")
  end
  protected
    def collection
      @product_registrations ||= end_of_association_chain.paginate(:page => params[:page])
    end
end