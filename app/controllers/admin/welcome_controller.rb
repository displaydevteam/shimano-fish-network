class Admin::WelcomeController < Admin::AdminController
  def home
    @total_users = Fisher.count
    @users_who_registered = ProductRegistration.pluck(:fisher_id).uniq.count
    @total_product_registrations = ProductRegistration.count
    @last_20_answers = Answer.published.order('created_at DESC').limit(20)
  end
end
