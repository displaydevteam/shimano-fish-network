class Admin::QuestionSubcategoriesController < Admin::AdminController
  inherit_resources
  belongs_to :question_category

  def create
    create!(:notice => 'Sottocategoria di domande creata con successo!') do
      admin_question_category_question_subcategories_path(@question_category)
    end
  end

  def update
    update!(:notice => 'Sottocategoria di domande aggiornata con successo!') do
      admin_question_category_question_subcategories_path(@question_category)
    end
  end

  def destroy
    destroy!(:notice => 'Sottocategoria di domande rimossa con successo!') do
      admin_question_category_question_subcategories_path(@question_category)
    end
  end

  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @question_subcategories ||= end_of_association_chain.paginate(:page => params[:page])
    end

end
