class Admin::QuestionsController < Admin::AdminController
  inherit_resources
  belongs_to :question_subcategory

  def update
    update!(:notice => 'Domanda aggiornata con successo!') do
      admin_question_category_question_subcategory_questions_path(@question_subcategory.question_category, @question_subcategory)
    end
  end

  def destroy
    destroy!(:notice => 'Domanda eliminata con successo!') do
      admin_question_category_question_subcategory_questions_path(@question_subcategory.question_categoru, @question_subcategory)
    end
  end

  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @questions ||= end_of_association_chain.paginate(:page => params[:page])
    end
end
