class Admin::AssistenceRequestsController < Admin::AdminController
  inherit_resources
  belongs_to :fisher, :product_registration
  
  def create
    create!(:notice => "Richiesta di assistenza creata con successo!") do
      admin_fisher_product_registration_assistence_requests_path(@fisher, @product_registration)
    end
  end
  
  def update
    update!(:notice => "Richiesta di assistenza aggiornata con successo!") do 
      admin_fisher_product_registration_assistence_requests_path(@fisher, @product_registration)
    end
  end
  
  def destroy
    destroy!(:notice => "Richiesta di assistenza eliminata con successo!") do 
      admin_fisher_product_registration_assistence_requests_path(@fisher, @product_registration)
    end
  end
  
  protected
    def collection
      @assistence_requests ||= end_of_association_chain.paginate(:page => params[:page])
    end
end