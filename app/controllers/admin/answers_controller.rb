class Admin::AnswersController < Admin::AdminController
  inherit_resources
  belongs_to :question_category
  belongs_to :question_subcategory
  belongs_to :question
  
  def update
    update!(:notice => 'Risposta aggiornata con successo!') do
      admin_question_category_question_subcategory_question_answers_path(@question_category, @question_subcategory, @question)
    end
  end

  def destroy
    destroy!(:notice => 'Risposta eliminata con successo!') do
      admin_question_category_question_subcategory_question_answers_path(@question_category, @question_subcategory, @question)
    end
  end

  protected
    def collection
      @q = end_of_association_chain.search(params[:q])
      @answers ||= end_of_association_chain.paginate(:page => params[:page])
    end
end
