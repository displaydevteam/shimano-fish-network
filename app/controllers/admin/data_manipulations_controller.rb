class Admin::DataManipulationsController < Admin::AdminController
  http_basic_authenticate_with :name => "shimano", :password => "shimanopassword"

  def show
    model_name = params[:id].to_s
    model_class = model_name.classify.constantize
    if DataManipulation::ALLOWED_MODELS.include? model_name.to_sym
      Mime::Type.register "application/vnd.ms-excel", :xls
      @records = model_class.all
      send_data  @records.to_xls(:columns => model_class.exportable_attributes, :headers => model_class.exportable_attributes.collect{|a| a.to_s.humanize})
    else
      render :file => "#{Rails.root}/public/404.html", :status => 404
    end
  end
  
  def index 
    @data_manipulation = DataManipulation.new()
  end
  
  def create
    @data_manipulation = DataManipulation.new(params[:data_manipulation])
    if @data_manipulation.process
      redirect_to admin_data_manipulations_path, :notice => 'Elaborazione completata con successo!'
    else
      render :action => :index
    end
  end
  
end
