# encoding: UTF-8
class Admin::PhotosController < Admin::AdminController
  inherit_resources
  belongs_to :gallery

  def update
    update!(:notice => 'Foto moderata con successo!'){ admin_gallery_path(@gallery) }
  end

  def destroy
    CommunityPhotoMailer.photo_refused(resource).deliver
    destroy!(:alert => 'Foto eliminata con successo!')
  end

  def publish
    if update_resource(resource, [:published => true] )
      redirect_to admin_gallery_path(resource.gallery), :notice => 'Foto pubblicata con successo!'
    else
      redirect_to admin_gallery_path(resource.gallery), :alert => 'Non è stato possibile pubblicare la foto'
    end
  end

  private
  def update_resource(object, attributes)
    object.assign_attributes *attributes, :as => :admin
    if object.changes['published'] == [false, true]
      CommunityPhotoMailer.photo_approved(object).deliver
    end
    object.save
  end
end
