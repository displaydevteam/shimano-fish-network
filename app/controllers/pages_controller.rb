class PagesController < ApplicationController
  STATIC_PAGES = %w(azienda cookies consigli-e-trucchi faq stampa_garanzia infoprivacy community_home soon green-line)
  
  def show
    if STATIC_PAGES.include? params[:page]
      render params[:page]  
    else
      render :file => "#{Rails.root}/public/404.html", :status => 404
    end
  end

  def perch
  	@articles = PostCategory.where(slug: 'perch').first.posts
  end

  def perch_show
    @article = Post.find_by_url(params[:slug])
  end
end
