class PostTagsController < ApplicationController
  def index
    @tags = PostTag.order("name")
    respond_to do |format|
      format.json{ render :json => @tags.tokens(params[:q]) }
    end
  end
end
