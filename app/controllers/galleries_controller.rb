class GalleriesController < ApplicationController
  def show
    if params[:id].present?
      @gallery = Gallery.find(params[:id])
    else
      @gallery = Gallery.order('created_at DESC').limit(1).first
    end
    @page_title = "Le foto della community"
    @meta_description = "Diventa protagonista della nostra community caricando le foto delle tue catture con attrezzatura Shimano, Loomis e Rapala"
  end
  
  def index
    @galleries = Gallery.order('created_at DESC')
    @page_title = "Le foto della community"
    @meta_description = "Diventa protagonista della nostra community caricando le foto delle tue catture con attrezzatura Shimano, Loomis e Rapala"
  end

end
