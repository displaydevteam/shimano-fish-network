class PostsController < ApplicationController
  include ActionView::Helpers::TextHelper
  def index
    @top3 = Post.where(:highlight => true).order("date DESC").limit(3)
    @random = Post.where(:highlight => true).shuffle.first
  	@cat = PostCategory.order("title")
  	@tags = PostTag.order("count DESC").limit(20)
    @posts = Post.published.order("date DESC")
    @posts = @posts.search(params[:q]) if params[:q].present?
  	@posts = @posts.paginate(:per_page => 24, :page => params[:page])
  end
  def category
    @random = Post.where(:highlight => true).shuffle.first
  	@cat = PostCategory.order("title")
  	@tags = PostTag.order("count DESC").limit(20)
  	@category = PostCategory.find_by_slug(params[:slug])
  	@posts = Post.published.where(:post_category_id => @category.id).order("date DESC").paginate(:per_page => 24, :page => params[:page])
  end

  def tag
    @random = Post.where(:highlight => true).shuffle.first
  	@cat = PostCategory.order("title")
  	@tags = PostTag.order("count DESC").limit(20)
  	@tag = PostTag.find_by_url(params[:url])
  	@posts = @tag.posts.published.order("date DESC").paginate(:per_page => 24, :page => params[:page])
  end

  def show
    @random = Post.where(:highlight => true).shuffle.first
    @cat = PostCategory.order("title")
  	@tags = PostTag.order("count DESC").limit(20)
    if params[:date].present?
  	 @post = Post.where(:date => params[:date]).where(:url => params[:title]).first
    else
      redirect_to post_show_path("#{params[:year]}-#{params[:day]}-#{params[:month]}",params[:title])
    end
    @page_title = @post.title
  end
end
