class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :http_auth
  before_filter :force_italian_locale
  before_filter :get_footer
  before_filter :check_path_and_redirect
  after_filter :store_location
  before_filter :accept_cookie

  def accept_cookie
    if cookies[:accept_cookiez] == 'true'
      @pop = false
    else
      @pop = true
      cookies.permanent[:accept_cookiez] = 'true'
    end
  end

  def store_location
    # store last url as long as it isn't a /users path
    session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
  end


  
  protected


  def get_footer
    if HomepageContent.count > 0
    @meta_image = HomepageContent.highlights.published.order('position ASC, id DESC').last.image.url
  end
    @product_types_f = ProductType.order("position")
    @questions_f = Question.limit(5)
    @last_blog = Post.published.order("date DESC").first(5)

  end
  
    def http_auth
      if Rails.env == 'staging'
        authenticate_or_request_with_http_basic do |username, password|
          username == "shimano" && password == "pesci"
        end
      end
    end

    def force_italian_locale
      I18n.locale = :it
    end

    private
  def check_path_and_redirect
    path = request.path
    new_path = CGI::unescape(request.path)
    redirect_to new_path, :status => 301 if new_path != request.path
    true
  end
end
