class StoresController < ApplicationController
  before_filter :fix_params
  respond_to :html, :json
  def index
    @regions = get_regions
    @regions = @regions.uniq.sort
    @provinces = get_provinces(params[:store][:region])
    @provinces  = @provinces.uniq.sort
    @cities = get_cities(params[:store][:region], params[:store][:province])
    @cities = @cities.uniq.sort
    if params[:specialist]
    @stores = get_special_stores(params[:store])
    else
    @stores = get_stores(params[:store])
    end
    @page_title = "Shimano shop"
    @meta_description = "Trova il negozio Shimano Shop piu vicino a te e specializzato nella vendita di prodotti per la pesca"
      if params[:store][:region].present?
        @page_title = "Shimano shop in " + params[:store][:region]
        @meta_description = "Trova il negozio Shimano Shop in" + params[:store][:region]
      end
      if params[:store][:city].present?
        @page_title = "Shimano shop a " + params[:store][:city]
        @meta_description = "Trova il negozio Shimano Shop a" + params[:store][:city]
      end
  end
  def suggest
    stores = Store.arel_table
    @stores = Store.visible.where(stores[:name].matches("%#{params[:term]}%"))
  end
  
  def get_regions
    Store.visible.regions.collect{|region| region.titleize}
  end
  
  def get_provinces(region)
    Store.visible.provinces(region)
  end
  
  def get_cities(region, province)
    Store.visible.cities(region, province)
  end

   def get_sregions
    Store.visible.where(params[:specialist]).regions.collect{|region| region.titleize}
  end
  
  def get_sprovinces(region)
    Store.visible.where(params[:specialist]).provinces(region)
  end
  
  def get_scities(region, province)
    Store.where(params[:specialist]).visible.cities(region, province)
  end
  
  protected
    
    def get_stores(options = {})
      return Store.visible.where(options).order("region ASC, province ASC, name ASC").paginate(:per_page => 20, :page => params[:page]) || []
    end

    def get_special_stores(options = {})
      return Store.where(params[:specialist]).where(options).order("region ASC, province ASC, name ASC").paginate(:per_page => 20, :page => params[:page]) || []
    end

  
    def fix_params
      params[:store] ||= {}
      params[:store] = params[:store].select{|k,v| v.present? }
    end
end
