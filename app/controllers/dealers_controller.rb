class DealersController < ApplicationController
  before_filter :authenticate_dealer!
  def index
  end

  def new
  	@dealer = DealerAssistanceRequest.new()
    @dealer.date = Date.today()
  	@dealer.dealer_id = current_dealer.id
  end

  def show
    @dealer = DealerAssistanceRequest.find(params[:id])
  end

  def create 

  	@dealer = DealerAssistanceRequest.new(params[:dealer_assistance_request])
  	if @dealer.save
      if @dealer.product.product_type == "Mulinelli"
        ProductRegistrationMailer.assistance('loris.fasani@shimano-eu.com',@dealer).deliver
      else
        ProductRegistrationMailer.assistance('assistenza-sif@shimano-eu.com ',@dealer).deliver
      end
  		redirect_to '/dealers'
  	else
      render 'new'
    end
  end
end
