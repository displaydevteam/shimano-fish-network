class HomeController < ApplicationController
require 'nokogiri'
before_filter :authenticate_fisher!, :except => [:import, :index, :store_contest]
skip_before_filter :verify_authenticity_token

  def index
    if params[:import_dealers].present?
      Dealer.import
    end
  	@highlights = HomepageContent.highlights.published.limit(5).order('position ASC, id DESC')
    @video = HomepageContent.where(title: 'video').order('position ASC, id DESC').first
    @pro = QuestionSubcategory.prostaff.shuffle.first
    @last_event = Event.order('start_date').last
    if @last_event.nil?
      @last_event = Event.last
    end
    @photo = Photo.where("homepage = ?", true).shuffle.first
    @last_question = Question.first
    @post = Post.published.order("date DESC").first
    @rpost = Post.where(:highlight => true).shuffle.first
    @last_gallery = Gallery.order('created_at DESC').limit(1).first
    @page_title = "Canne e Mulinelli da Pesca"
    @meta_description = "Scopri le canne, i mulinelle da pesca e tutti i prodotti del Shimano Fish Network."
  end

  def import 
    Fisher.import_c
    redirect_to root_path
  end  

  def clean_p
    Product.select{|p| p.code.nil?}.destroy_all
    redirect_to root_path
  end 

  def store_contest
    contest = ShimanoContestUser.new()
    contest.email = params[:email]
    contest.form_data = params[:form_data]
    if contest.save
      render :json => 'ok'
    else
      render :json => 'ko'
    end
  end
end