# encoding: UTF-8
class QuestionsController < ApplicationController
  before_filter :authenticate_fisher!
  before_filter :get_category, :except => [:search]

  def index
    sort_by = params[:show] == 'latest' ? 'updated_at' : 'created_at'
    subcategory_id= QuestionSubcategory.find_by_url(params[:subcategory_url]).try(:id)
    @questions = Question.where(:question_subcategory_id => subcategory_id || @subcategories.pluck(:id))
    @questions = @questions.search(params[:q]) if params[:q].present?
    @questions = @questions.published.order("#{sort_by} DESC")
              .paginate(:per_page => 10, :page => params[:page])
    category_id = QuestionCategory.find_by_url(params[:category_url]).try(:id)
    @subcategories = QuestionSubcategory.published.where(:question_category_id => category_id).order('name')
    @page_title = @category.name 
    @meta_description = @category.description
  end

  def expert
    category_id = QuestionCategory.find_by_url(params[:category_url]).try(:id)
    @prostaff = QuestionSubcategory.published.where(:question_category_id => category_id)
  end
  
  def new
    subcategory = QuestionSubcategory.find_by_url(params[:subcategory_url])
    @question = Question.new :question_subcategory => subcategory
  end
  
  def search
    sort_by = params[:show] == 'latest' ? 'updated_at' : 'created_at'
    redirect_to ask_shimano_path and return unless params[:q].present?
    @questions = Question.published
    @questions = @questions.search(params[:q]) if params[:q].present?
    @questions = @questions.order("#{sort_by} DESC")
                           .paginate(:per_page => 10, :page => params[:page])
  end

  def show
    session["fisher_return_to"] = request.path unless signed_in?
    @question = Question.find(params[:id])
    if @question.published?
      @answers = @question.answers.published.order('created_at ASC').paginate(:per_page => 20, :page => params[:page])
      @most_liked_answers = @question.answers.most_liked
      @answer = Answer.new :question_id => @question.id, :fisher_id => current_fisher.id if fisher_signed_in?
    else
      raise ActiveRecord::RecordNotFound
    end
    @page_title = @question.title 
  end

  def like_answer
    @answer = Answer.find(params[:answer_id])
    session[:answers] ||= []
    notice = "Hai già votato questa risposta"
    unless session[:answers].include? @answer.id
      @answer.likes = @answer.likes.to_i + 1
      session[:answers] << @answer.id
      @answer.save
      notice = "Il tuo voto è stato registrato con successo!"
    end
    redirect_to question_path(:id => @answer.question), :notice => notice
  end

  def create_answer
    @question = Question.find(params[:id])
    @answers = @question.answers.published.order('created_at ASC')
    @answer = Answer.new params[:answer]
    @answer.question = @question
    @answer.fisher = current_fisher
    @answer.published = true
    if @answer.save
      AskShimanoMailer.new_answer(@answer).deliver
      AskShimanoMailer.new_answerexpert(@answer).deliver
      redirect_to "#{question_path(:id => @question, :subcategory_url => @question.question_subcategory.url, :category_url => @question.question_subcategory.question_category.url)}#answer_#{@answer.id}", :notice => 'Risposta aggiunta con successo!'
    else
      render :action => :show
    end
  end

  def create
    @question = Question.new(params[:question])
    @question.published = true
    @question.fisher = current_fisher
    if @question.save
      AskShimanoMailer.new_question(@question).deliver if @question.question_subcategory.email.present?
      redirect_to questions_path(:category_url => @category.url), :notice => 'Domanda creata con successo!'
    else
      render :action => :new
    end
  end
  
  protected

  def get_category
    @category = QuestionCategory.find_by_url(params[:category_url])
    @subcategories = @category.question_subcategories.published
  end
end
