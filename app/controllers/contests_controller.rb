class ContestsController < ApplicationController
  def home
  end

  def play

  end

  def result

  end

  def index
  	@contests = Contest.all
  	if fisher_signed_in?
  		arr = []
  		@contests.each do |c|
  			if current_fisher.voted_for? c 
  				arr << c
  			end
  		end
  		if arr.size > 0
  			@arr = false
  		else 
  			@arr = true
  		end
  	end
  end

  def vote
  	contest = Contest.find(params[:id])
  	current_fisher.likes contest
  	redirect_to contest_path

  end
end
