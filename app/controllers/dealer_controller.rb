class DealerController < ApplicationController
  before_filter :authenticate_dealer!
  def index
  	@downloads = DealerAttachment.all
  end

  def download
  	require 'uri'
    require 'aws-sdk-s3'
    file = DealerAttachment.find(params[:file])
    s = URI(URI.decode(file.file)).path
    s3_client = Aws::S3::Client.new
    @download = Aws::S3::Object.new(
        key: s[1..-1] , bucket_name: 'shmnassets', client: s3_client).presigned_url(:get, expires_in: 60 * 60
    )
    redirect_to  @download

  end
end

