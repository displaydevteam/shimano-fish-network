class ProductFinderController < ApplicationController
  def index
  	if params[:slug].present?
      url = params[:slug].split('/').last
  		@answer = ProductFinderAnswer.find_by_url(url)
      if @answer.next_question.present?
  		  @question = @answer.next_question
      else 
        @products = @answer.product_finder_products.order("ordering ASC")
      end
  	else
  		@question = ProductFinderQuestion.where(:product_finder_answer_id => nil).first
  	end
  end

  def show
  	@answer = ProductFinderAnswer.find_by_url(params[:a1_id3])
  	@products = @answer.product_finder_products
  end

  def refactor
    ProductFinderAnswer.all.each do |p|
      p.url = p.title + "-" + p.id.to_s
      p.save
    end
    redirect_to root_path
  end
end
