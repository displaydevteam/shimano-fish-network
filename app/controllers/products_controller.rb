class ProductsController < ApplicationController
  respond_to :html, :json
  
  def suggest
    query = params[:term]
    @products = Product.where(:product_type => params[:product_type], :hidden => false).where{description.matches("%#{query}%") | code.matches("%#{query}%")}
  end
  
  def list 
  	if params[:q].length > 2
  	@q = Product.ransack(:description_cont => params[:q])
  	@results = @q.result(distinct: true)
  	else
  	@q = Product.ransack(:description_cont => params[:q]).limit(10)
  	@results = @q.result(distinct: true)
  	end	
  end

  def dealer
    if params[:q].present?
    @q = Product.ransack(:description_or_code_cont => params[:q])
    @results = @q.result(distinct: true)
    else
    @results = Product.last(10)
    end 
  end
end
