class ProstaffController < ApplicationController
  
  def index
  	@page_title = "Shimano Pro Staff"
  	@meta_description = "Una squadra di esperti pescatori nelle varie tecniche di pesca che ci aiuteranno a creare e sviluppare prodotti sempre nuovi ed all'avanguardia."
  	@prostaff = QuestionSubcategory.prostaff.all
  end
  def show
  	@prostaff = QuestionSubcategory.find_by_url(params[:url])
  	@page_title = @prostaff.name
  	@meta_description = @prostaff.bio.squish
  end
end
