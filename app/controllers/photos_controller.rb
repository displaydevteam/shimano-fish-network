# encoding: UTF-8
class PhotosController < ApplicationController
  before_filter :get_photo
  
  def show
    @page_title = @photo.title
    @meta_description = @photo.description
  end

  def like
    session[:photos] ||= []
    notice = 'Hai già votato questa foto'
    unless session[:photos].include? @photo.id
      @photo.likes = @photo.likes.to_i + 1 
      session[:photos] << @photo.id
      @photo.save
      notice = 'Il tuo voto è stato registrato con successo!'
    end
    redirect_to photo_path(@gallery.id, @photo.id), :notice => notice
  end

  protected
  def gallery
    @gallery ||= Gallery.find(params[:gallery_id])
  end

  def get_photo
    @photo ||= gallery.photos.published.find(params[:id])
  end
end
