class QuestionCategoriesController < ApplicationController
  def index
    @question_categories = QuestionCategory.scoped.order('position')
  end
end
