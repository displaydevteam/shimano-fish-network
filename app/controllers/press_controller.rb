class PressController < ApplicationController
  def index
  	@catalogues = Catalogue.order("position")
  	@page_title = "Cataloghi per la pesca"
  	@meta_description = "Tutti i cataloghi per la pesca Shimano disponibili per il download gratuito"
  end
end
