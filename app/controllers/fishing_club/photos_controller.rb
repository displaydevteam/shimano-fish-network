class FishingClub::PhotosController < ApplicationController
  before_filter :authenticate_fisher!

  def new
    @photo = Photo.new
    @photo.agreement = false
    @photo.regulation = false
    @photo.gallery = active_gallery
    render :file => "#{Rails.root}/public/404.html", :status => 404 if active_gallery.nil?
  end

  def create
    @photo = Photo.new(params[:photo])
    @photo.gallery_id = active_gallery.id
    @photo.fisher_id = current_fisher.id
    if @photo.save
      CommunityPhotoMailer.admin_notice(@photo).deliver
      redirect_to fishing_club_path, :notice => 'Foto caricata con successo. Ora aspetta che venga approvata!'
    else
      render :action => :new
    end
  end

  private
  def active_gallery
    @active_gallery ||= Gallery.order('created_at DESC').limit(1).first
  end

end
