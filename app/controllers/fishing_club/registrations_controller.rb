class FishingClub::RegistrationsController < Devise::RegistrationsController
  
  def new
    @fisher = Fisher.new
    @fisher.privacy = false
    @fisher.newsletter = false
    @fisher.third_party = false
  end
  
  def edit
  end
  def update
    fisher = Fisher.find(current_fisher.id)
    if fisher.update_without_password(params[:fisher])
      redirect_to fishing_club_path, :notice => 'Dati personali aggiornati con successo!'
    else
      render :action => :edit
    end
  end
end