class FishingClub::SessionsController < Devise::SessionsController
  before_filter :fix_params

  def new
  	@page_title = "Garanzia"
  	@meta_description = "Registra le tue canne e i tuoi mulinelli da pesca Shimano e Gloomis in pochi clic, abilitando la garanzia per il terzo anno."
  end
  protected
    def fix_params
      params[:fisher] = params[:session] unless params[:fisher].present?
    end
end