class FishingClub::ProductRegistrationsController < ApplicationController
  before_filter :authenticate_fisher!, :fix_params, :set_filters
  
  def index
    @product_registrations = current_fisher.product_registrations
  end
  
  def new
    session[:product_registration] = nil
    @product_registration = ProductRegistration.new(:fisher => current_fisher, :purchase_date => Date.today)
  end
  
  def confirm
    @product_registration = ProductRegistration.new(params[:product_registration])
    render :action => :new unless @product_registration.valid?
  end

  def edit
    @product_registration = ProductRegistration.find(params[:id])
  end
  
  def update
    @product_registration = ProductRegistration.find(params[:id])
    @product_registration.update_attributes(params[:product_registration])
    
    if @product_registration.fisher.campaign_2
      ProductRegistrationMailer.campaign_2(@product_registration).deliver
    end
    redirect_to fishing_club_path, notice: 'Prodotto aggiornato'
  end

  def create
    @product_registration = ProductRegistration.new(params[:product_registration])
    current_fisher.product_registrations << @product_registration
    if current_fisher.save
      ProductRegistrationMailer.successful_registration(@product_registration).deliver
      redirect_to fishing_club_path, :notice => I18n.t('product_registrations.created')
    else
      render :action => :new
    end
  end
  
  def show
    @product_registration = ProductRegistration.find(params[:id])
    render :layout => 'garanzia'
  end
  
  protected
    def fix_params
      params[:store] ||= {}
      params[:product] ||= {}
    end

    def set_filters
      @product_types = Product.product_types.uniq
      if params[:product][:product_type].present?
        @families = Product.families(params[:product][:product_type])
      end
      if @families.present? and params[:product][:family].present?
        @categories = Product.categories(params[:product][:product_type], params[:product][:family])
      end
      if @categories.present? and (params[:product][:category].present? or @categories.compact.count <= 1)
        @products = Product.where(params[:product])
      end
    end
    
    
end