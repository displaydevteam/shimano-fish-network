class FishingClub::FishingClubController < ApplicationController
  before_filter :authenticate_fisher!
  def index
      @highlights = HomepageContent.highlights.published.limit(5).order('position ASC, id DESC')
    @video = HomepageContent.video.order('position ASC, id DESC').first
    @pro = QuestionSubcategory.prostaff.shuffle.first
    @last_event = Event.order('start_date').last
    if @last_event.nil?
      @last_event = Event.last
    end
    @photo = Photo.where("homepage = ?", true).shuffle.first
    @last_question = Question.first
    @post = Post.published.order("date DESC").first
    @rpost = Post.where(:highlight => true).shuffle.first
    @last_gallery = Gallery.order('created_at DESC').limit(1).first
    @page_title = "Canne e Mulinelli da Pesca"
    @meta_description = "Scopri le canne, i mulinelle da pesca e tutti i prodotti del Shimano Fish Network."
  	@documents = Document.order("title ASC")
    @photos = current_fisher.photos
    @product_registrations = current_fisher.product_registrations.order('purchase_date DESC')
  end
  def product
  	@documents = Document.order("title ASC")
    @tickets = PrizeTicket.where(email: current_fisher.email)
    @photos = current_fisher.photos
    @product_registrations = current_fisher.product_registrations.order('purchase_date DESC')
  end
end
