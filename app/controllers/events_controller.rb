class EventsController < ApplicationController
  before_filter :authenticate_fisher!

  def reserve
    @stores = Store.where(reserve: true)
    if @stores.count == 0 
      Store.update_from_csv
    end
  end

  def index
  	@events = Event.where('end_date > ?', Date.today).order('start_date')
    @page_title = "Eventi"
    @meta_description = "Resta aggiornato sugli eventi che Shimano organizza in tutta Italia per conoscerti di persona e per permetterti di toccare con mano i nostri prodotti."
  end

  def archive
    @events = Event.where('start_date < ?', Date.today).order('start_date DESC')
  end

  def show
  	@event = Event.find_by_id(params[:id])
    @shops = Store.visible

  end

  def list 
  	@event = Event.find params[:id]
  end

  def attend 
  	event = params[:id]
  	fisher = current_fisher.id
    store = Store.first
    code = (0...8).map { (65 + rand(26)).chr }.join
    if UserEvent.where(:event_id => event, :fisher_id => fisher).blank?
  	   @attend = UserEvent.new(:event_id => event, :fisher_id => fisher, :code => code, :store_id => store.id)
    else
       redirect_to events_path
    end
  	if @attend.save
      EventAttend.attend(@attend).deliver
  		redirect_to events_path
  	else 
  		render :action => :show
  	end
  end
end
