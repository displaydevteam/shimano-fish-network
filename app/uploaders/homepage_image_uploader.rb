# encoding: utf-8
class HomepageImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::CarrierWaveStoreDirConfig
  
    version :thumb do
    process :resize_to_limit => [220,220]
  end

  version :square do
    process :resize_to_fill => [600,600]
  end
  version :big do
    process :resize_to_limit => [1200,1200]
  end
  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
