# encoding: utf-8
class ExportUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "exports"
  end

end

