# encoding: utf-8
class AttachUploader < CarrierWave::Uploader::Base 
  include CarrierWave::CarrierWaveStoreDirConfig

  def extension_white_list
    %w(jpg jpeg gif png pdf zip)
  end
end
