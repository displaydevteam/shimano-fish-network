class Document < ActiveRecord::Base
  attr_accessible :file_name, :sku, :title, :doc_type, :attachment
  #Descrizione,SKU,Nome File
  mount_uploader :attachment, AttachUploader

  def self.import
	    CSV.foreach('public/lista.csv', :headers => true ) do |row|
	    	Document.create(:title => row[0], :sku => row[1], :file_name => row[2], :doc_type => 'esploso' )
	    end # end CSV.foreach
	end # end self.import(file)
end
