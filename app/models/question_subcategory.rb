class QuestionSubcategory < ActiveRecord::Base
  belongs_to :question_category
  has_many :questions, :dependent => :destroy
  before_validation :set_url

  scope :published, where(:published => true)
  scope :prostaff, where(:prostaff => true).where(:published => true)

  validates :name, :presence => true
  validates :url, :uniqueness => { :scope => :question_category_id }, :presence => true
  mount_uploader :image, PhotoUploader
  private
  def set_url
    self.url = self.name.parameterize
  end
end
