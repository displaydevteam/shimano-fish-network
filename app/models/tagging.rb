class Tagging < ActiveRecord::Base
  belongs_to :catalogue_item
  belongs_to :tag
end
