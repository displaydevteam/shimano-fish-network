class DataManipulation
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  ALLOWED_MODELS = [:products, :stores, :fishers, :product_registrations]
  
  attr_accessor :file, :model_name
  
  def initialize(options = {})
    self.file = options[:file]
    self.model_name = options[:model_name].try(:singularize)
  end
  
  def process
    self.records.each do |record|
      begin
        if record[:id].present?
          item = model_name.classify.constantize.find(record[:id].to_i)
          item.update_attributes(record)
          item.save
        else
          raise ActiveRecord::RecordNotFound
        end
      rescue
        item = model_name.classify.constantize.create(record)
      end
    end
    return true
  end
  
  def records
    headers = nil
    rows = []
    spreadsheet = Spreadsheet.open(self.file.path)
    unless spreadsheet.worksheets.empty? 
      spreadsheet.worksheets.first.each do |row|
        if headers.nil?
          headers = row.collect{|field| field.parameterize('_').strip}
        else
          rows << Hash[headers.zip(row.collect{|field| field.to_s.strip })].symbolize_keys
        end
      end
    end
    return rows
  end
  
  
  def persisted?
    false
  end
end