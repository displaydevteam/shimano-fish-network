class QuestionCategory < ActiveRecord::Base
  before_validation :set_url
  
  has_many :question_subcategories, :dependent => :destroy
  has_many :questions, :through => :question_subcategories

  validates :name, :presence => true
  validates :url, :uniqueness => true, :presence => true


  def questions_count
    Question.published.where(:question_subcategory_id => self.question_subcategories.published.pluck(:id)).count
  end

  private
  def set_url
    self.url = self.name.parameterize
  end

end
