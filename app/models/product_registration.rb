class ProductRegistration < ActiveRecord::Base
  @@agreement = false
  START_DATE = '2012-01-01'
  belongs_to :fisher, :counter_cache => :products_count
  belongs_to :product
  belongs_to :store

  mount_uploader :receipt_photo, PhotoUploader
  mount_uploader :product_photo, PhotoUploader

  attr_accessor :agreement
  has_many :assistence_requests, :dependent => :destroy
  
  validates :fisher_id, :product_id, :store_id, :purchase_date, :presence => true
  validates :agreement, :acceptance => true, :if => lambda{ self.errors.empty? }
  validates_each :purchase_date do |obj, attr, value|
    submitted_date = (value || '1970-01-01').to_date
    if submitted_date < '2012-01-01'.to_date || submitted_date > Date.today
      obj.errors.add(attr, I18n.t('activerecord.errors.models.product_registration.attributes.purchase_date.blank'))
    end
  end

  after_validation :set_warranty_expiration_date
  delegate :family, :category, :product_type, :code, :description, :to => :product, :prefix => false, :allow_nil => true
  delegate :name, :surname, :email, :to => :fisher, :prefix => true, :allow_nil => true
  
  def self.exportable_attributes
    [:code, :product_type, :category, :family, :description, :store_name, :purchased_at, :fisher_name, :fisher_surname, :fisher_email]
  end
  
  def purchased_at
    I18n.l(self.purchase_date, :format => :short)
  end

  def store_name
    self.store.name
  end
  
  def warranty_expired?
    self.warranty_expiration_date < Date.today
  end

  def to_s
    "#{self.fisher.complete_name} - #{self.product}"
  end
  
  protected
    def set_warranty_expiration_date
      if self.warranty_expiration_date.blank?
      self.warranty_expiration_date = self.purchase_date + 3.years if self.purchase_date.present?
    end
    end
  
end
