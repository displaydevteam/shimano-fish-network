class PrizeTicket < ActiveRecord::Base
  attr_accessible :code, :email
  before_validation :set_access_token
	def set_access_token
		unless code.present?
	    self.code = "LM-#{generate_token}"
	   	end
	end

	  def generate_token
	    loop do
	      token = SecureRandom.hex(4)
	      break token unless PrizeTicket.where(code: token).exists?
	    end
	  end
end
