class ProductImage < ActiveRecord::Base
  mount_uploader :image, PhotoUploader
  belongs_to :catalogue_item
  default_scope order('id')
end
