class Contest < ActiveRecord::Base
  attr_accessible :body, :title, :image, :attachment
  acts_as_votable
  mount_uploader :image, PhotoUploader
  mount_uploader :attachment, AttachUploader
end
