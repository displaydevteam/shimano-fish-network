class ProductFinderProduct < ActiveRecord::Base
  attr_protected
  has_many :product_finder_productanswers
  has_many :product_finder_answers, :through => :product_finder_productanswers
end
