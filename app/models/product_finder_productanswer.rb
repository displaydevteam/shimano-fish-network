class ProductFinderProductanswer < ActiveRecord::Base
  attr_accessible :product_finder_answer_id, :product_finder_product_id
  belongs_to :product_finder_answer
  belongs_to :product_finder_product

  def to_s
  	self.product_finder_answer.title
  end
end
