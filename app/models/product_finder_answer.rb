class ProductFinderAnswer < ActiveRecord::Base
  mount_uploader :image, PhotoUploader
  attr_accessible :next_question_id, :product_finder_question_id, :title, :url, :image
  has_one :product_finder_question

  has_many :product_finder_productanswers
  has_many :product_finder_products, :through => :product_finder_productanswers

  before_validation :set_url

  def set_url
  	self.url = self.title.parameterize.to_s + "-" + self.id.to_s
  end
  def next_question
    self.product_finder_question
  end

  def products
  end
end
