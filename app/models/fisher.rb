class Fisher < ActiveRecord::Base
  @@privacy = false
  @@newsletter = false
  @@third_party = false
  has_many :user_events
  has_many :product_registrations
  has_many :events, through: :user_events
  before_validation :calculate_year
  acts_as_voter

  include Gravtastic
  gravtastic

  SEA_TECHNIQUES = ["Spinning", "Traina costiera", "Traina d'altura",
                    "Surf casting", "Pesca da riva in mare (moli/scogli)",
                    "Vertical jigging", "Light jigging (inchiku/kabura)",
                    "Pesca da natante"]
  INNER_WATERS_TECHNIQUES = ["Spinning", "Trota laghetto", "Trota torrente",
                             "Carp fishing laghetto", "Carp fishing fiume/lago",
                             "Pesca con bolognese/passata", "Roubaisienne", "Mosca", "Feeder"]

  JOBS = ["Impiegato", "Operaio", "Pensionato",
          "Studente", "Imprenditore", "Agente di commercio",
          "Libero professionista", "Dirigente", "Altro"]

  REGIONS = ["Valle d'Aosta", "Piemonte", "Liguria",
             "Friuli Venezia Giulia", "Toscana", "Veneto",
             "Lombardia", "Emilia Romagna", "Lazio",
             "Umbria", "Marche", "Trentino Alto Adige",
             "Abruzzo", "Molise", "Campania", "Puglia",
             "Basilicata", "Calabria", "Sicilia", "Sardegna"]

  def calculate_year
    if self.year_of_birth.nil?
      self.year_of_birth = self.birth_date.year
    end
  end

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  attr_accessible :name, :surname, :year_of_birth,
                  :region, :email, :password,
                  :password_confirmation, :remember_me, :job, :agonist,
                  :inner_waters_techniques, :sea_techniques, :privacy,
                  :newsletter, :third_party, :birth_date, :mobile_number, :address,
                  :campaign_1, :campaign_2, :campaign_3, :campaign_4

  validates :name, :surname,
            :region, :email, :job, :presence => true
  validates :agonist, :inclusion => [true, false]
  validates :privacy, :newsletter, :third_party, :acceptance => { :accept => "true" }, :on => :create

  validates_each :techniques do |record, attr, value|
    record.errors.add(attr, "Dicci almeno una tecnica") if value.select { |t| t.present? }.empty?
  end

  serialize :sea_techniques
  serialize :inner_waters_techniques

  has_many :product_registrations, :dependent => :destroy
  has_many :products, :through => :product_registrations
  has_many :photos
  has_many :questions

  #after_save :check_newsletter_subscription

  def self.exportable_attributes
    return [:name, :surname, :year_of_birth, :birth_date, :mobile_number, :address, :registered_at,
            :region, :email, :job, :agonist, :products_count] + technique_fields
  end

  def self.mailchimp_attributes
    return [:name, :surname, :year_of_birth,
            :region, :email, :job, :agonist, :products_count] + technique_fields
  end

  def self.technique_fields
    (INNER_WATERS_TECHNIQUES.map { |t| "iw_#{t}".parameterize("_") if t.present? } + SEA_TECHNIQUES.map { |t| "s_#{t}".parameterize("_") if t.present? }).collect { |t| t.parameterize("_").to_sym }
  end

  def technique_fields
    @technique_fields ||= begin
        self.class.technique_fields
      end
  end

  def registered_at
    I18n.l(self.created_at, :format => :short)
  end

  def method_missing(method_name, *args, &block)
    if self.technique_fields.include? method_name.to_sym
      return !!(self.all_techniques =~ /#{method_name.to_s}/)
    end
    super(method_name, *args, &block)
  end

  def techniques
    (self.sea_techniques || []).map { |t| "s_#{t}" if t.present? } + (self.inner_waters_techniques || []).map { |t| "iw_#{t}" if t.present? }
  end

  def all_techniques
    @all_techniques ||= begin
        self.techniques.collect { |t| t.to_s.parameterize("_") }.join(", ")
      end
  end

  def complete_name
    "#{self.name} #{self.surname}".strip
  end

  def privacy_name
    "#{self.name} #{self.surname[0]}."
  end

  def to_s
    self.complete_name
  end

  def newsletter_proxy
    NEWSLETTER
  end

  def check_newsletter_subscription
    #self.newsletter_proxy.subscribe_user(self) if self.active_for_authentication?
  end

  def destroy
    #self.newsletter_proxy.unsubscribe_user(self) if self.active_for_authentication?
    super
  end

  def self.import_c
    require "csv"
    CSV.foreach("camp_2.csv") do |row|
      u = Fisher.find_by_email(row[0].strip)
      if u.present?
        u.campaign_2 = true
        u.save
      end
    end
  end
end
