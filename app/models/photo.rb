class Photo < ActiveRecord::Base
  @@agreement = false 
  @regulation = false

  belongs_to :gallery
  belongs_to :fisher
  
  attr_accessible :title, :description, :image, :agreement, :regulation, :as => [:default, :admin]
  attr_accessible :published, :cover, :fisher_id, :homepage, :gallery_id, :likes, :as => :admin

  validates :title, :description, :presence => true
  validates :agreement, :regulation, :acceptance => { :accept => 'true' }, :on => :create

  scope :published, where(:published => true)
  scope :cover, where(:cover => true).limit(1)

  mount_uploader :image, PhotoUploader

  def next
    self.gallery.photos.published.where("id > ?", self.id).order('id ASC').first
  end

  def prev
    self.gallery.photos.published.where("id < ?", self.id).order('id DESC').first
  end
end
