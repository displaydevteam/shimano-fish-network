class Dealer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :chatbot, :email, :password, :password_confirmation, :remember_me, :store_id, :dealer_code
  # attr_accessible :title, :body
  belongs_to :store
  has_many :dealer_assistance_requests
  def to_s
    self.email
  end

  def self.import
    CSV.foreach('res.csv', :headers => true ) do |row|
      d = Dealer.find_or_create_by_email(row[2].strip)
      d.dealer_code = row[0].strip
      d.email = row[2].strip
      d.chatbot = true
      d.password = "ShimanoDealer2019"
      d.save
    end
  end
end
