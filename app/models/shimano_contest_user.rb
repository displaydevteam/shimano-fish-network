class ShimanoContestUser < ActiveRecord::Base
  attr_accessible :email, :form_data
  serialize :form_data
end
