class ProductFinderQuestion < ActiveRecord::Base
  attr_accessible :root, :title, :url
  belongs_to :product_finder_answer
  has_many :product_finder_answers
  attr_protected

  def answers
    self.product_finder_answers
  end

  def parent_answer
    self.product_finder_answer
  end
end
