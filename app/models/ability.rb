class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new # guest user (not logged in)
    case user.role
    when 'admin' then can :manage, :all
    when 'moderator' then can :manage, Question
    when 'assistancer' then can :manage, Fisher; can :manage, ProductRegistration; can :manage, AssistenceRequest
    end
  end
end
