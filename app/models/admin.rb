class Admin < ActiveRecord::Base
  devise :database_authenticatable, :recoverable, :rememberable
         
  attr_accessible :email, :password, :password_confirmation, :remember_me
end
