class Gallery < ActiveRecord::Base
  has_many :photos
  
  validates :title, :description, :presence => true

  def cover
    self.photos.cover
  end
end
