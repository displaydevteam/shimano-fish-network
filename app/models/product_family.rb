class ProductFamily < ActiveRecord::Base
  belongs_to :product_type
  has_many :catalogue_items, :dependent => :destroy

  validates :name, :description, :product_type, :presence => true
  validates :url, :presence => true, :uniqueness => true

  default_scope :order => 'name ASC'
end
