class HomepageContent < ActiveRecord::Base
  mount_uploader :image, HomepageImageUploader
  scope :highlights, where(:highlight => true)
  scope :normal, where(:highlight => false)
  scope :published, where(:published => true)
  scope :video, where("homepage_contents.video IS NOT NULL")
  validates :title, :abstract, :link, :presence => true
  validates :image, :presence => true, :if => :highlight?
end