class ProductType < ActiveRecord::Base
  has_many :product_families, :dependent => :destroy

  validates :name, :description, :presence => true
  validates :url, :uniqueness => true, :presence => true

  default_scope :order => 'position ASC'
end
