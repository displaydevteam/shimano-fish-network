class Catalogue < ActiveRecord::Base
  attr_accessible :image, :link, :position, :title, :attachments_attributes
  mount_uploader :image, PhotoUploader
  has_many :attachments, :as => :item
  accepts_nested_attributes_for :attachments, :allow_destroy => true
end
