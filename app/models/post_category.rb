class PostCategory < ActiveRecord::Base
  attr_accessible :slug, :title
  has_many :posts
end
