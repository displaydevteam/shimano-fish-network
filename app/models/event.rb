class Event < ActiveRecord::Base
  attr_accessible :description, :end_date, :image, :link, :start_date, :title, :partecipate
  mount_uploader :image, PhotoUploader
  has_many :user_events
  has_many :fishers, through: :user_events
end
