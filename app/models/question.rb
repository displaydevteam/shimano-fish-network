class Question < ActiveRecord::Base
  include ::ShimanoFishNetwork::Validators

  @username = nil
  attr_accessor :username

  belongs_to :question_subcategory
  belongs_to :fisher
  has_many :answers, :dependent => :destroy

  scope :published, where(:published => true)
  scope :unpublished, where{published != true }

  default_scope order('created_at DESC')

  validates :title, :question, :question_subcategory, :presence => true
  validates :username, :absence => true # honeypot for spam bots

  def self.searchable_language
    'italian'
  end

  def to_param
    "#{self.id}-#{self.title.parameterize}"
  end

  def fisher_privacy_name
    fisher.try(:privacy_name) || 'Utente Ospite'
  end

  def fisher_complete_name
    fisher.try(:complete_name) || 'Utente Ospite'
  end
end
