class Attachment < ActiveRecord::Base
  attr_accessible :title, :url, :item_id, :item_type, :link
  mount_uploader :url, AttachUploader
  belongs_to :item, :polymorphic => true
end
