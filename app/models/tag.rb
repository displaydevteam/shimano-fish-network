class Tag < ActiveRecord::Base
  before_validation :set_url
  validates :name, :uniqueness => true, :presence => true

  has_many :catalogue_items, :through => :taggings
  has_many :taggings, :dependent => :destroy

  default_scope :order => 'name ASC'

  def self.tokens(query)
    tags = where("name like ?", "%#{query}%")
    if tags.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      tags
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens.split(',')
  end

  private
  def set_url
    self.url = self.name.parameterize
  end
end
