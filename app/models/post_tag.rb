class PostTag < ActiveRecord::Base
  before_validation :set_url
  before_validation :set_count
  validates :name, :uniqueness => true, :presence => true

  has_many :posts, :through => :post_taggings
  has_many :post_taggings, :dependent => :destroy

  attr_reader :tag_tokens

  def self.updatecount
    PostTag.all.each do |tag|
      count = tag.posts.count
      tag.count = count
      tag.save
    end
  end
  def self.tokens(query)
    tags = where("name like ?", "%#{query}%")
    if tags.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      tags
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens.split(',')
  end

  private
  def set_url
    self.url = self.name.parameterize
  end

  def set_count
    self.count = self.posts.count
  end
end
