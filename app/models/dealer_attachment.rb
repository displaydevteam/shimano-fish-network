class DealerAttachment < ActiveRecord::Base
  validates :title, :file, presence: true
end
