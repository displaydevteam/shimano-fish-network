class Store < ActiveRecord::Base
  validates :name, :address, :region, 
            :province, :city, :cap, 
            :phone, :presence => true
  
  has_many :product_registrations, :dependent => :destroy

  after_validation :set_visiblility
  
  scope :visible, where(:hidden => false)
  
  before_validation :generate_country

  serialize :geo_data
  
  def lat_lng
      if geo_data.present?
        geo_data.first["geometry"]["location"]
      end
  end

  def self.update_from_csv
    require 'csv'    
    #206868,"RENZI LINO, DOGANA REP. SAN MARINO",VIA 3  SETTEMBRE N 126,47891,DOGANA REP. SAN MARINO,EE ,renzicacciapesca@alice.sm  
    csv_text = File.read('stores.csv')
    csv = CSV.parse(csv_text, :headers => false)
    no_data = []
    csv.each do |row|
      cap = row[3].strip
      email = row[6].strip
      sname = row[1].downcase
      saddress = row[2].downcase
      s = Store.where(cap: row[3])
      if s.count == 1 
        s.first.reserve = true
        s.first.email = email
        s.first.save
      else
        no_data << row[1]
      end
    end
    puts no_data
  end

  def nice_url
    unless self.website[/\Ahttp:\/\//] || self.website[/\Ahttps:\/\//]
      "http://#{self.website}"
    else
      self.website
    end
  end

  def self.exportable_attributes
    self.column_names.select{|name| not name =~ /(updated|created)_at/}
  end
  
  def self.regions
    self.uniq.pluck(:region)
  end          
  
  def self.cities(region = nil, province = nil)
    self.uniq.select(:city).where(:province => province, :region => region).map(&:city)
  end
  
  def self.provinces(region = nil)
    self.uniq.select(:province).where(:region => region).map(&:province)
  end
  
  def complete_address
    "#{self.address}, #{self.cap} #{self.city} - #{self.province}"
  end

  def half_address
    "#{self.cap} #{self.city} - #{self.province}"
  end

  def expert
    self.boat || self.bass || self.surf || self.spinningsw || self.spinningfw || self.pesca_al_colpo || self.carpfishing || self.trota_lago
  end
  
  def to_s
    "#{self.name} - #{self.complete_address}".strip
  end
  
  def gmaps_address
    "#{self.address}, #{self.city}, #{self.cap}, Italy"
  end

  def generate_country
    if self.address.present?
      address = URI.encode("#{self.address} + #{self.city}")
      result = JSON.load(open("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&key=AIzaSyAD-8y-r80aw9kMjNARzKgvhb5LBHhk-Y8"))
      puts result
      if result["results"].present?
        self.geo_data = result["results"]
       else
        self.geo_data = nil
      end
    end
  end
  
  protected
    def set_visiblility
      self.hidden = false if self.hidden.nil?
    end  
end
