class DealerAssistanceRequest < ActiveRecord::Base
	belongs_to :dealer
	belongs_to :product
	before_validation :set_status
	after_create :set_sku
	has_many :dealer_assisteance_request_products
	accepts_nested_attributes_for :dealer_assisteance_request_products, :allow_destroy => true
	mount_uploader :receipt, PhotoUploader
	mount_uploader :picture, PhotoUploader
	validates :product_id, :description, presence: true
	validates :receipt, presence: true, if: :has_receipt?
	validates :picture, presence: true, if: :product_rod?
 
	def has_receipt?
		with_receipt
	end

	def product_rod?
		self.product.product_type == "Canne Da Pesca"
	end



	def set_sku
		sku = self.dealer.dealer_code + '-' + self.date.strftime("%Y") + '-' + self.id.to_s
		self.update_attribute("sku",sku)
	end

	def set_status
		unless self.status
			self.status = 'inviata'
		end
	end

	def self.statuses
		["inviata","respinta","in carico","ultimata"]
	end
end
