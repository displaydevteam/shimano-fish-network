class ProductAttachment < ActiveRecord::Base
  belongs_to :catalogue_item
  mount_uploader :url, AttachUploader
  validates :url, :presence => true
end
