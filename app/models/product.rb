class Product < ActiveRecord::Base
  validate :product_type, :family, :code, :description, :presence => true
  validate :code, :uniqueness => true
  has_many :product_registrations, :dependent => :destroy
  has_many :fishers, :through => :product_registrations
  
  def self.exportable_attributes
    self.column_names.select{|name| not name =~ /(updated|created)_at/}
  end
  
  def self.product_types
    self.uniq.pluck(:product_type)
  end
  
  def self.families(product_type)
    self.where(:product_type => product_type).uniq.pluck(:family)
  end
  
  def self.categories(product_type, family)
    self.where(:product_type => product_type, :family => family).uniq.pluck(:category)
  end
  
  def to_s
    "#{self.description} (cod. #{self.code})"
  end
  
end
