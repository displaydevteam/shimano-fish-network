class Answer < ActiveRecord::Base
  include ::ShimanoFishNetwork::Validators

  @username = nil
  attr_accessor :username

  belongs_to :fisher
  belongs_to :question, :touch => true

  validates :text, :presence => true
  validates :username, :absence => true #honeypot for spam bots

  scope :published, where(:published => true)
  scope :unpublished, where{published != true }
  scope :most_liked, where{ likes > 0 }.order('likes DESC, id DESC').limit(1)

  def fisher_privacy_name
    fisher.try(:privacy_name) || 'Utente Ospite'
  end
end
