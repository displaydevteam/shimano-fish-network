class CatalogueItem < ActiveRecord::Base
  belongs_to :product_family

  has_many :tags, :through => :taggings
  has_many :taggings, :dependent => :destroy
  has_many :product_images
  has_many :product_attachments

  scope :published, where("hide != ? or hide is null", true)

  attr_accessible :name, :url, :product_family_id, :hide, :description, :sizes, :tag_ids, :tag_tokens, :product_images_attributes, :product_attachments_attributes

  attr_reader :tag_tokens

  validates :name, :url, :product_family_id, :description, :sizes, :presence => true
  accepts_nested_attributes_for :product_images, :reject_if => lambda { |a| a['image'].blank? }, :allow_destroy => true
  accepts_nested_attributes_for :product_attachments, :reject_if => lambda {|a| a['url'].blank? }, :allow_destroy => true
  
  def self.searchable_language
    'italian'
  end

  def tag_tokens=(tokens)
    self.tag_ids = Tag.ids_from_tokens(tokens)
  end
  
end
