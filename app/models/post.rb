class Post < ActiveRecord::Base
  attr_accessible :post_category_id, :highlight, :date, :description, :published, :title, :word_id, :url, :image, :remote_image_url, :post_tag_ids, :tag_tokens, :fisher_content, :remove_image
  belongs_to :post_category
  has_many :post_tags, :through => :post_taggings
  has_many :post_taggings, :dependent => :destroy
  before_validation :set_url
  mount_uploader :image, PhotoUploader

  attr_reader :tag_tokens

  def tag_tokens=(tokens)
    self.post_tag_ids = PostTag.ids_from_tokens(tokens)
  end

  def self.published
    where("date <= ?", DateTime.now).where(published: true)
  end

  def self.updatedate 
  	f = File.open("public/sfn.xml")
	doc = Nokogiri::XML(f)
	f.close
	data_nodes = doc.css("item")
	data_nodes.each do |item|
		if Post.where(:word_id == item.xpath("wp:post_id").text.to_i ).count > 0
			date = item.xpath("pubDate").text.to_date
			post = Post.where(:word_id == item.xpath("wp:post_id").text.to_i ).first
			post.update_attributes(:date => date)
			post.save
		end
	end
  end

  def self.updateimage
  	Post.all.each do |post|
  		html = Nokogiri::HTML.fragment(post.description)
  		if html.css('img').first.class.to_s != 'NilClass'
  			source = html.css('img').first.attr('src')
  			post.remote_image_url = source
  			post.save
        puts source
  		end
  	end
  end

  def set_url
    unless self.url.present?
  	 self.url = self.title.parameterize
    end
  end

  def self.fix_desc
  	  Post.all.each do |post|
  	  	  description = auto_html(post.description.gsub("[youtube=",'').gsub("]",'')) { 
		    	youtube(:width => 400, :height => 250, :autoplay => false)
		        simple_format
    		}
    	  post.update_attributes(:description => description)
    	  post.save
      end
  end

  def self.seturls
  	Post.all.each do |post|
  		post.update_attributes(:url => post.title.parameterize)
  	end
  end

  def self.setimages
  	Post.all.each do |post|
  		post.update_attributes(:url => post.title.parameterize)
  	end
  end
  def self.wordpressimport
  	f = File.open("public/sfn_update.xml")
	doc = Nokogiri::XML(f)
	f.close
	data_nodes = doc.css("item")
	data_nodes.each do |item|
				if item.xpath("wp:post_type").text == 'post'
					item.xpath("category[@domain='category']").each do |cat|
						if PostCategory.where(:title => cat.text).count == 1
							@shit = PostCategory.where(:title => cat.text).last.id
						end
					end
					title = item.xpath("title").text
					date = item.xpath("wp:post_date").text
					description = item.xpath("content:encoded").text
					published = true
					word_id = item.xpath("wp:post_id").text.to_i
					post = Post.create(:title => title, :post_category_id => @shit, :date => date, :description => description, :published => true, :word_id => word_id)
					item.xpath("category[@domain='post_tag']").each do |cat|
						if PostTag.where(:name => cat.text).count == 1
							tag = PostTag.where(:name => cat.text).first
							PostTagging.create(:post_id => post.id, :post_tag_id => tag.id)
						else
							tag = PostTag.create(:name => cat.text)
							PostTagging.create(:post_id => post.id, :post_tag_id => tag.id)
						end
					end
				end
			end
  end

  def self.importcat
  	f = File.open("public/sfn_update.xml")
	doc = Nokogiri::XML(f)
	f.close
	data_nodes = doc.css("item")
	data_nodes = doc.xpath("//wp:category")
	data_nodes.each do |cat|
		if cat.xpath("wp:category_parent").text == ''
			PostCategory.create(:title => cat.xpath("wp:cat_name").text, :slug => cat.xpath("wp:category_nicename").text)
		end
	end
  end
end
