class UserEvent < ActiveRecord::Base
  attr_accessible :event_id, :fisher_id, :code, :store_id
  belongs_to :event
  belongs_to :fisher
  belongs_to :store
end
