require 'spec_helper'

describe "admin/fishers/new" do
  before(:each) do
    assign(:admin_fisher, stub_model(Admin::Fisher).as_new_record)
  end

  it "renders new admin_fisher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => admin_fishers_path, :method => "post" do
    end
  end
end
