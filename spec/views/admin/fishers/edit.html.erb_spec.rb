require 'spec_helper'

describe "admin/fishers/edit" do
  before(:each) do
    @admin_fisher = assign(:admin_fisher, stub_model(Admin::Fisher))
  end

  it "renders the edit admin_fisher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => admin_fishers_path(@admin_fisher), :method => "post" do
    end
  end
end
