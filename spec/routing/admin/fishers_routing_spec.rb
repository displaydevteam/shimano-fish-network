require "spec_helper"

describe Admin::FishersController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/fishers").should route_to("admin/fishers#index")
    end

    it "routes to #new" do
      get("/admin/fishers/new").should route_to("admin/fishers#new")
    end

    it "routes to #show" do
      get("/admin/fishers/1").should route_to("admin/fishers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/fishers/1/edit").should route_to("admin/fishers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/fishers").should route_to("admin/fishers#create")
    end

    it "routes to #update" do
      put("/admin/fishers/1").should route_to("admin/fishers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/fishers/1").should route_to("admin/fishers#destroy", :id => "1")
    end

  end
end
