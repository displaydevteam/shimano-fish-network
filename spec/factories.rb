FactoryGirl.define do
  
  factory :fisher do
    sequence(:name) { |n| "Fisher #{n} name" }
    sequence(:surname) { |n| "Fisher #{n} name" }
    year_of_birth{ 1900 + rand(Time.now.year - 1900) }
    sequence(:email) { |n| "fisher#{n}@email.it" }
    sequence(:password) {|n| "password" }
    sequence(:password_confirmation) {|n| "password" }
    sequence(:region) {|n| "Region #{n}" }
    inner_waters_techniques Fisher::INNER_WATERS_TECHNIQUES
    sea_techniques Fisher::SEA_TECHNIQUES
    sequence(:job) {|n| "JOB #{n}" }
    agonist false
  end
  
end
