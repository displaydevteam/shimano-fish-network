module Registration
  
  def fill_fisher_form(fisher, fill_all = true)
    fields = %w(name surname email password password_confirmation year_of_birth city)
    number_of_fields = fill_all ? fields.length : rand(fields.length) 
    fields_to_fill_in = fields.sample(number_of_fields)
    within('#new_fisher') do 
      fields_to_fill_in.each do |field|
        fill_in "fisher_#{field}", :with => fisher.send(field)
      end
    end
  end
  
end