require 'spec_helper.rb'

describe "Fishing club's" do
  
  before(:each) do 
    host! 'Shimano-fish-network.dev'
    Capybara.app_host = "http://Shimano-fish-network.dev"
    reset_email
  end
  
  context "pages" do 
    it "should not be accessible by anonymous users" do 
      visit fishing_club_path
      current_path.should eq(new_fisher_session_path)
    end
  end
  
  context "registration process" do
    it "should not complete without all mandatory fields filled in" do
      visit new_fisher_registration_path
      fill_fisher_form(FactoryGirl.build(:fisher), false)
      click_button "fisher_registration_submit"
      page.should have_content "obbligatorio"
      email_count.should eq(0)
    end
    
    it "should complete when all mandatory fields are filled in" do
      visit new_fisher_registration_path
      fill_fisher_form(FactoryGirl.build(:fisher), true)
      click_button "fisher_registration_submit"
      page.should have_content I18n.t('devise.registrations.signed_up_but_unconfirmed')
      email_count.should eq(1)      
    end
    
    it "should alert for bad email" do
      visit new_fisher_registration_path
      fill_fisher_form(FactoryGirl.build(:fisher, :email => 'not an email'), true)
      click_button "fisher_registration_submit"
      page.should have_content I18n.t('activerecord.errors.models.fisher.attributes.email.invalid')
      email_count.should eq(0)
    end
    
  end
  
  
  
  
end