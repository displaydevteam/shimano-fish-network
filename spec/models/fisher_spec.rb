require 'spec_helper'

describe Fisher do
  before(:all) do
    @fisher = FactoryGirl.build :fisher
    NEWSLETTER.reset
  end

  it "should have a fake newsletter proxy" do
    @fisher.newsletter_proxy.class.should eq(ShimanoFishNetwork::FakeNewsletter)
  end

  it "should be added to newsletter after confirmation" do
    @fisher.save
    NEWSLETTER.get_user_info(@fisher.email)['success'].should eq(0)
    @fisher.confirm!
    NEWSLETTER.get_user_info(@fisher.email)['success'].should eq(1)
  end

  it "should be removed from newsletter after being destroyed" do
    @fisher.save
    Fisher.count.should eq(1)
    @fisher.confirm!
    email = @fisher.email
    NEWSLETTER.get_user_info(email)['success'].should eq(1)
    @fisher.destroy
    NEWSLETTER.get_user_info(email)['success'].should eq(0)
    Fisher.count.should eq(0)
  end

end
