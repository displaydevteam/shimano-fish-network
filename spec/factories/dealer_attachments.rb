# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dealer_attachment do
    title "MyString"
    file "MyString"
    published false
  end
end
