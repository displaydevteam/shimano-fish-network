# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :document do
    title "MyString"
    sku "MyString"
    file_name "MyString"
    type ""
  end
end
