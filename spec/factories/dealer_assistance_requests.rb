# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dealer_assistance_request do
    sku "MyString"
    store_id 1
    date "2016-05-30"
    product_id 1
    warranty false
    receipt false
    receipt_date "2016-05-30"
    client_full_name "MyString"
    client_email "MyString"
    problem_description "MyText"
    request_part "MyText"
  end
end
