# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :catalogue do
    title "MyString"
    image "MyString"
    position 1
    link "MyString"
  end
end
