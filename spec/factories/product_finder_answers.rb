# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_finder_answer do
    product_finder_question_id 1
    title "MyString"
    url "MyString"
    next_question_id 1
  end
end
