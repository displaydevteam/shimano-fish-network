# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :shimano_contest_user do
    email "MyString"
    form_data "MyText"
  end
end
