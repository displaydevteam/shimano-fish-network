# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :prize_ticket do
    email "MyString"
    code "MyString"
  end
end
