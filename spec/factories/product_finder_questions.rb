# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_finder_question do
    title "MyString"
    url "MyString"
    root false
  end
end
