source "https://rubygems.org"

gem "rails", "~> 3.2"
ruby "2.2.2"

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem "aws-sdk-s3", "~> 1.30"
gem "net-ssh", "~> 2.9"
gem "best_in_place"
gem "devise", "~> 3.5"
gem "simple_form", "~> 1.5"
gem "cocoon", "~> 1.2"
gem "feedjira", "~> 2.0"
gem "jquery-rails", "~> 2.3"
gem "jquery-ui-rails", "~> 5.0"
gem "will_paginate", "~> 3.0"
gem "will_paginate-bootstrap", "~> 1.0"
gem "less-rails", "~> 2.6"
gem "twitter-bootstrap-rails", "~> 2.2"
gem "carrierwave", "~> 0.10"
gem "therubyracer", "~> 0.12"
gem "libv8", "~> 3.16"
gem "inherited_resources", "1.6.0"
gem "mini_magick", "~> 4.3"
gem "cancan", "~> 1.6"
gem "fog", "~> 1.34"
gem "ransack", "~> 1.7"
gem "s3_direct_upload", "~> 0.1"
gem "sprockets-rails"
gem "bourbon", "4.2.4"
gem "turbo-sprockets-rails3", "~> 0.3"
gem "to_xls", "~> 1.5"
gem "rabl", "~> 0.11"
gem "squeel", "~> 0.8"
gem "gravtastic", "~> 3.2"
gem "rails_autolink", "~> 1.1"
gem "activeadmin", "1.0.0.pre1"
gem "ckeditor", "~> 4.1"
gem "sass-rails", "~> 3.2"
gem "formtastic", "~> 3.1"
gem "texticle", "~> 2.2", :require => "texticle/rails"
gem "pg", "~> 0.18"
gem "gibbon", "~> 0.4"
gem "typhoeus", "~> 0.4"
gem "nokogiri", "~> 1.6"
gem "auto_html", "~> 1.6"
gem "select2-rails", "~> 4.0"
gem "activeadmin-select2", "0.1.6"
gem "yui-compressor", "~> 0.12"
gem "rails-i18n", "~> 3.0" # For 3.x
gem "autoprefixer-rails", "~> 6.0"
gem "font-awesome-rails", "~> 4.4"
gem "test-unit", "~> 3.1"
gem "rails4-autocomplete", "~> 1.1"
gem "whenever", "~> 0.9", :require => false
gem "acts_as_votable", "~> 0.10"
# Gems used only for assets and not required
# in production environments by default.
# group :assets do
#   gem 'sass-rails',   '~> 3.2.3'
#   gem 'coffee-rails', '~> 3.2.1'
#
#   # See https://github.com/sstephenson/execjs#readme for more supported runtimes
#   # gem 'therubyracer'
#
#   gem 'uglifier', '>= 1.0.3'
# end
gem "passenger", ">= 5.3.2", require: "phusion_passenger/rack_handler"

group :development, :test do
  gem "taps", "~> 0.3"
  gem "heroku", "~> 3.41"
  gem "sqlite3", "~> 1.3"
  gem "capistrano-rails", "~> 1.1", require: false
  gem "capistrano-faster-assets", "~> 1.1", require: false
  gem "capistrano-bundler", "~> 1.1", require: false
  gem "rspec-rails", "~> 2.8"
  gem "capybara", "~> 2.5"
  gem "factory_girl_rails", "~> 1.3"
  gem "database_cleaner", "~> 1.5"
  gem "spork", "~> 1.0.0rc4"
  gem "log_buddy", "~> 0.7"
  gem "timecop", "~> 0.8"
  gem "simplecov", "~> 0.10", :require => false
  gem "launchy", "~> 2.4"
  gem "capistrano-local-precompile", "~> 1.0", require: false
end

group :production do
  gem "thin", "~> 1.6"
  gem "uglifier", "~> 2.7"
end

group :assets do
  gem "chosen-rails", "~> 1.4"
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the web server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'ruby-debug19', :require => 'ruby-debug'
