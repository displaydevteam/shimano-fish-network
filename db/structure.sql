--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    resource_id character varying(255) NOT NULL,
    resource_type character varying(255) NOT NULL,
    author_id integer,
    author_type character varying(255),
    body text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    namespace character varying(255)
);


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE admin_users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    role character varying(255)
);


--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admin_users_id_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admin_users_id_seq OWNED BY admin_users.id;


--
-- Name: admins; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE admins (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admins_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admins_id_seq OWNED BY admins.id;


--
-- Name: answers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE answers (
    id integer NOT NULL,
    text text,
    question_id integer,
    fisher_id integer,
    published boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    likes integer
);


--
-- Name: answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE answers_id_seq OWNED BY answers.id;


--
-- Name: assistence_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE assistence_requests (
    id integer NOT NULL,
    product_registration_id integer,
    started_at date,
    finished_at date,
    notes text,
    created_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    updated_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone
);


--
-- Name: assistence_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE assistence_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: assistence_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE assistence_requests_id_seq OWNED BY assistence_requests.id;


--
-- Name: attachments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE attachments (
    id integer NOT NULL,
    title character varying(255),
    url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    item_id integer,
    item_type character varying(255),
    link character varying(255)
);


--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attachments_id_seq OWNED BY attachments.id;


--
-- Name: catalogue_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE catalogue_items (
    id integer NOT NULL,
    name character varying(255),
    url character varying(255),
    description text,
    sizes text,
    product_family_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hide boolean
);


--
-- Name: catalogue_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE catalogue_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: catalogue_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE catalogue_items_id_seq OWNED BY catalogue_items.id;


--
-- Name: catalogues; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE catalogues (
    id integer NOT NULL,
    title character varying(255),
    image character varying(255),
    "position" integer,
    link character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: catalogues_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE catalogues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: catalogues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE catalogues_id_seq OWNED BY catalogues.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ckeditor_assets (
    id integer NOT NULL,
    data_file_name character varying(255) NOT NULL,
    data_content_type character varying(255),
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: dealer_assistance_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE dealer_assistance_requests (
    id integer NOT NULL,
    sku character varying(255),
    dealer_id integer,
    date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status character varying(255)
);


--
-- Name: dealer_assistance_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE dealer_assistance_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dealer_assistance_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE dealer_assistance_requests_id_seq OWNED BY dealer_assistance_requests.id;


--
-- Name: dealer_assisteance_request_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE dealer_assisteance_request_products (
    id integer NOT NULL,
    dealer_assistance_request_id integer,
    product_id integer,
    warranty boolean,
    receipt boolean,
    receipt_date date,
    client_full_name character varying(255),
    client_email character varying(255),
    problem_description text,
    request_part text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: dealer_assisteance_request_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE dealer_assisteance_request_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dealer_assisteance_request_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE dealer_assisteance_request_products_id_seq OWNED BY dealer_assisteance_request_products.id;


--
-- Name: dealers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE dealers (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    store_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: dealers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE dealers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dealers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE dealers_id_seq OWNED BY dealers.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE documents (
    id integer NOT NULL,
    title character varying(255),
    sku character varying(255),
    file_name character varying(255),
    doc_type character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    attachment character varying(255)
);


--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE documents_id_seq OWNED BY documents.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE events (
    id integer NOT NULL,
    title character varying(255),
    description text,
    start_date date,
    end_date date,
    link character varying(255),
    image character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    partecipate boolean
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: fishers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fishers (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying(255),
    surname character varying(255),
    year_of_birth character varying(255),
    region character varying(255),
    job character varying(255),
    agonist boolean,
    sea_techniques text,
    inner_waters_techniques text,
    products_count integer DEFAULT 0,
    birth_date date,
    mobile_number character varying(255),
    address text
);


--
-- Name: fishers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fishers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fishers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fishers_id_seq OWNED BY fishers.id;


--
-- Name: galleries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE galleries (
    id integer NOT NULL,
    title character varying(255),
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: galleries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE galleries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: galleries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE galleries_id_seq OWNED BY galleries.id;


--
-- Name: homepage_contents; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE homepage_contents (
    id integer NOT NULL,
    title character varying(255),
    abstract text,
    link character varying(255),
    image character varying(255),
    highlight boolean,
    published boolean,
    new_window boolean,
    "position" integer DEFAULT 0,
    created_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    updated_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    video character varying(255)
);


--
-- Name: homepage_contents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE homepage_contents_id_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: homepage_contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE homepage_contents_id_seq OWNED BY homepage_contents.id;


--
-- Name: photos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE photos (
    id integer NOT NULL,
    fisher_id integer,
    gallery_id integer,
    title character varying(255),
    description text,
    published boolean DEFAULT false,
    image character varying(255),
    likes integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cover boolean DEFAULT false,
    homepage boolean
);


--
-- Name: photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE photos_id_seq OWNED BY photos.id;


--
-- Name: post_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE post_categories (
    id integer NOT NULL,
    title character varying(255),
    slug character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: post_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE post_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE post_categories_id_seq OWNED BY post_categories.id;


--
-- Name: post_taggings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE post_taggings (
    id integer NOT NULL,
    post_id integer,
    post_tag_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: post_taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE post_taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE post_taggings_id_seq OWNED BY post_taggings.id;


--
-- Name: post_tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE post_tags (
    id integer NOT NULL,
    name character varying(255),
    url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    count integer
);


--
-- Name: post_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE post_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE post_tags_id_seq OWNED BY post_tags.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE posts (
    id integer NOT NULL,
    post_category_id integer,
    date date,
    title character varying(255),
    description text,
    published boolean,
    word_id integer,
    image character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    url character varying(255),
    fisher_content text,
    highlight boolean
);


--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- Name: product_attachments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_attachments (
    id integer NOT NULL,
    catalogue_item_id integer,
    url character varying(255),
    label character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: product_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_attachments_id_seq OWNED BY product_attachments.id;


--
-- Name: product_families; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_families (
    id integer NOT NULL,
    product_type_id integer,
    name character varying(255),
    url character varying(255),
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: product_families_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_families_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_families_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_families_id_seq OWNED BY product_families.id;


--
-- Name: product_finder_answers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_finder_answers (
    id integer NOT NULL,
    product_finder_question_id integer,
    title character varying(255),
    url character varying(255),
    next_question_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image character varying(255)
);


--
-- Name: product_finder_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_finder_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_finder_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_finder_answers_id_seq OWNED BY product_finder_answers.id;


--
-- Name: product_finder_productanswers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_finder_productanswers (
    id integer NOT NULL,
    product_finder_answer_id integer,
    product_finder_product_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: product_finder_productanswers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_finder_productanswers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_finder_productanswers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_finder_productanswers_id_seq OWNED BY product_finder_productanswers.id;


--
-- Name: product_finder_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_finder_products (
    id integer NOT NULL,
    product_finder_answer_id integer,
    fishing_reel_name character varying(255),
    product_type character varying(255),
    fishing_reel_link character varying(255),
    image character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fishing_rod_name character varying(255),
    fishing_rod_link character varying(255),
    fishing_reel_image character varying(255),
    fishing_rod_image character varying(255),
    ordering integer
);


--
-- Name: product_finder_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_finder_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_finder_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_finder_products_id_seq OWNED BY product_finder_products.id;


--
-- Name: product_finder_questions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_finder_questions (
    id integer NOT NULL,
    title character varying(255),
    url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    product_finder_answer_id integer
);


--
-- Name: product_finder_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_finder_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_finder_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_finder_questions_id_seq OWNED BY product_finder_questions.id;


--
-- Name: product_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_images (
    id integer NOT NULL,
    catalogue_item_id integer,
    image character varying(255),
    "position" integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- Name: product_registrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_registrations (
    id integer NOT NULL,
    product_id integer,
    fisher_id integer,
    store_id integer,
    purchase_date date,
    warranty_expiration_date date,
    created_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    updated_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone
);


--
-- Name: product_registrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_registrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_registrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_registrations_id_seq OWNED BY product_registrations.id;


--
-- Name: product_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_types (
    id integer NOT NULL,
    name character varying(255),
    url character varying(255),
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "position" integer
);


--
-- Name: product_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_types_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_types_id_seq OWNED BY product_types.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    product_type character varying(255),
    family character varying(255),
    category character varying(255),
    code character varying(255),
    description character varying(255),
    created_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    updated_at timestamp without time zone DEFAULT '2012-02-15 21:07:33'::timestamp without time zone,
    hidden boolean DEFAULT false
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 8830
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: question_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE question_categories (
    id integer NOT NULL,
    name character varying(255),
    description text,
    url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "position" integer DEFAULT 0,
    "group" character varying(255)
);


--
-- Name: question_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE question_categories_id_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: question_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE question_categories_id_seq OWNED BY question_categories.id;


--
-- Name: question_subcategories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE question_subcategories (
    id integer NOT NULL,
    question_category_id integer,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published boolean,
    url character varying(255),
    image character varying(255),
    bio text,
    email character varying(255),
    profile character varying(255),
    prostaff boolean
);


--
-- Name: question_subcategories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE question_subcategories_id_seq
    START WITH 28
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: question_subcategories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE question_subcategories_id_seq OWNED BY question_subcategories.id;


--
-- Name: questions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE questions (
    id integer NOT NULL,
    title character varying(255),
    question text,
    fisher_id integer,
    question_subcategory_id integer,
    published boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "position" integer DEFAULT 0
);


--
-- Name: questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE questions_id_seq OWNED BY questions.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: stores; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stores (
    id integer NOT NULL,
    name character varying(255),
    region character varying(255),
    province character varying(255),
    city character varying(255),
    address character varying(255),
    cap character varying(255),
    phone character varying(255),
    website character varying(255),
    created_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    updated_at timestamp without time zone DEFAULT '2012-02-15 21:07:31'::timestamp without time zone,
    hidden boolean DEFAULT false,
    boat boolean,
    surf boolean,
    spinningsw boolean,
    spinningfw boolean,
    bass boolean,
    pesca_al_colpo boolean,
    carpfishing boolean,
    trota_lago boolean,
    jp boolean,
    green boolean
);


--
-- Name: stores_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stores_id_seq OWNED BY stores.id;


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE taggings (
    id integer NOT NULL,
    catalogue_item_id integer,
    tag_id integer
);


--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying(255),
    url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: user_events; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_events (
    id integer NOT NULL,
    event_id integer,
    fisher_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    code character varying(255),
    store_id integer
);


--
-- Name: user_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_events_id_seq OWNED BY user_events.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_users ALTER COLUMN id SET DEFAULT nextval('admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins ALTER COLUMN id SET DEFAULT nextval('admins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY answers ALTER COLUMN id SET DEFAULT nextval('answers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY assistence_requests ALTER COLUMN id SET DEFAULT nextval('assistence_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments ALTER COLUMN id SET DEFAULT nextval('attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY catalogue_items ALTER COLUMN id SET DEFAULT nextval('catalogue_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY catalogues ALTER COLUMN id SET DEFAULT nextval('catalogues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY dealer_assistance_requests ALTER COLUMN id SET DEFAULT nextval('dealer_assistance_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY dealer_assisteance_request_products ALTER COLUMN id SET DEFAULT nextval('dealer_assisteance_request_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY dealers ALTER COLUMN id SET DEFAULT nextval('dealers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fishers ALTER COLUMN id SET DEFAULT nextval('fishers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY galleries ALTER COLUMN id SET DEFAULT nextval('galleries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY homepage_contents ALTER COLUMN id SET DEFAULT nextval('homepage_contents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY photos ALTER COLUMN id SET DEFAULT nextval('photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY post_categories ALTER COLUMN id SET DEFAULT nextval('post_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY post_taggings ALTER COLUMN id SET DEFAULT nextval('post_taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY post_tags ALTER COLUMN id SET DEFAULT nextval('post_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_attachments ALTER COLUMN id SET DEFAULT nextval('product_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_families ALTER COLUMN id SET DEFAULT nextval('product_families_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_finder_answers ALTER COLUMN id SET DEFAULT nextval('product_finder_answers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_finder_productanswers ALTER COLUMN id SET DEFAULT nextval('product_finder_productanswers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_finder_products ALTER COLUMN id SET DEFAULT nextval('product_finder_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_finder_questions ALTER COLUMN id SET DEFAULT nextval('product_finder_questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_registrations ALTER COLUMN id SET DEFAULT nextval('product_registrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_types ALTER COLUMN id SET DEFAULT nextval('product_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY question_categories ALTER COLUMN id SET DEFAULT nextval('question_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY question_subcategories ALTER COLUMN id SET DEFAULT nextval('question_subcategories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY questions ALTER COLUMN id SET DEFAULT nextval('questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stores ALTER COLUMN id SET DEFAULT nextval('stores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_events ALTER COLUMN id SET DEFAULT nextval('user_events_id_seq'::regclass);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: admins_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- Name: assistence_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY assistence_requests
    ADD CONSTRAINT assistence_requests_pkey PRIMARY KEY (id);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: catalogue_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY catalogue_items
    ADD CONSTRAINT catalogue_items_pkey PRIMARY KEY (id);


--
-- Name: catalogues_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY catalogues
    ADD CONSTRAINT catalogues_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: dealer_assistance_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY dealer_assistance_requests
    ADD CONSTRAINT dealer_assistance_requests_pkey PRIMARY KEY (id);


--
-- Name: dealer_assisteance_request_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY dealer_assisteance_request_products
    ADD CONSTRAINT dealer_assisteance_request_products_pkey PRIMARY KEY (id);


--
-- Name: dealers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY dealers
    ADD CONSTRAINT dealers_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: fishers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fishers
    ADD CONSTRAINT fishers_pkey PRIMARY KEY (id);


--
-- Name: galleries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY galleries
    ADD CONSTRAINT galleries_pkey PRIMARY KEY (id);


--
-- Name: homepage_contents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY homepage_contents
    ADD CONSTRAINT homepage_contents_pkey PRIMARY KEY (id);


--
-- Name: photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- Name: post_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY post_categories
    ADD CONSTRAINT post_categories_pkey PRIMARY KEY (id);


--
-- Name: post_taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY post_taggings
    ADD CONSTRAINT post_taggings_pkey PRIMARY KEY (id);


--
-- Name: post_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY post_tags
    ADD CONSTRAINT post_tags_pkey PRIMARY KEY (id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: product_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_attachments
    ADD CONSTRAINT product_attachments_pkey PRIMARY KEY (id);


--
-- Name: product_families_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_families
    ADD CONSTRAINT product_families_pkey PRIMARY KEY (id);


--
-- Name: product_finder_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_finder_answers
    ADD CONSTRAINT product_finder_answers_pkey PRIMARY KEY (id);


--
-- Name: product_finder_productanswers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_finder_productanswers
    ADD CONSTRAINT product_finder_productanswers_pkey PRIMARY KEY (id);


--
-- Name: product_finder_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_finder_products
    ADD CONSTRAINT product_finder_products_pkey PRIMARY KEY (id);


--
-- Name: product_finder_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_finder_questions
    ADD CONSTRAINT product_finder_questions_pkey PRIMARY KEY (id);


--
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- Name: product_registrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_registrations
    ADD CONSTRAINT product_registrations_pkey PRIMARY KEY (id);


--
-- Name: product_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_types
    ADD CONSTRAINT product_types_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: question_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY question_categories
    ADD CONSTRAINT question_categories_pkey PRIMARY KEY (id);


--
-- Name: question_subcategories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY question_subcategories
    ADD CONSTRAINT question_subcategories_pkey PRIMARY KEY (id);


--
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- Name: stores_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stores
    ADD CONSTRAINT stores_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: user_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_events
    ADD CONSTRAINT user_events_pkey PRIMARY KEY (id);


--
-- Name: idx_ckeditor_assetable; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable ON ckeditor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_ckeditor_assetable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable_type ON ckeditor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_admin_notes_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_admin_notes_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_email ON admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON admin_users USING btree (reset_password_token);


--
-- Name: index_admin_users_on_role; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_admin_users_on_role ON admin_users USING btree (role);


--
-- Name: index_admins_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admins_on_email ON admins USING btree (email);


--
-- Name: index_admins_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admins_on_reset_password_token ON admins USING btree (reset_password_token);


--
-- Name: index_answers_on_fisher_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_answers_on_fisher_id ON answers USING btree (fisher_id);


--
-- Name: index_answers_on_likes; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_answers_on_likes ON answers USING btree (likes);


--
-- Name: index_answers_on_published; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_answers_on_published ON answers USING btree (published);


--
-- Name: index_answers_on_question_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_answers_on_question_id ON answers USING btree (question_id);


--
-- Name: index_catalogue_items_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_catalogue_items_on_url ON catalogue_items USING btree (url);


--
-- Name: index_dealers_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_dealers_on_email ON dealers USING btree (email);


--
-- Name: index_dealers_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_dealers_on_reset_password_token ON dealers USING btree (reset_password_token);


--
-- Name: index_fishers_on_confirmation_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_fishers_on_confirmation_token ON fishers USING btree (confirmation_token);


--
-- Name: index_fishers_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_fishers_on_email ON fishers USING btree (email);


--
-- Name: index_fishers_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_fishers_on_reset_password_token ON fishers USING btree (reset_password_token);


--
-- Name: index_photos_on_fisher_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_photos_on_fisher_id ON photos USING btree (fisher_id);


--
-- Name: index_photos_on_gallery_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_photos_on_gallery_id ON photos USING btree (gallery_id);


--
-- Name: index_photos_on_published; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_photos_on_published ON photos USING btree (published);


--
-- Name: index_post_taggings_on_post_id_and_post_tag_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_post_taggings_on_post_id_and_post_tag_id ON post_taggings USING btree (post_id, post_tag_id);


--
-- Name: index_post_tags_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_post_tags_on_url ON post_tags USING btree (url);


--
-- Name: index_product_attachments_on_catalogue_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_attachments_on_catalogue_item_id ON product_attachments USING btree (catalogue_item_id);


--
-- Name: index_product_families_on_product_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_families_on_product_type_id ON product_families USING btree (product_type_id);


--
-- Name: index_product_families_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_families_on_url ON product_families USING btree (url);


--
-- Name: index_product_finder_questions_on_product_finder_answer_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_finder_questions_on_product_finder_answer_id ON product_finder_questions USING btree (product_finder_answer_id);


--
-- Name: index_product_images_on_catalogue_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_images_on_catalogue_item_id ON product_images USING btree (catalogue_item_id);


--
-- Name: index_product_registrations_on_fisher_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_registrations_on_fisher_id ON product_registrations USING btree (fisher_id);


--
-- Name: index_product_types_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_types_on_url ON product_types USING btree (url);


--
-- Name: index_products_on_category; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_category ON products USING btree (category);


--
-- Name: index_products_on_family; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_family ON products USING btree (family);


--
-- Name: index_products_on_hidden; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_hidden ON products USING btree (hidden);


--
-- Name: index_products_on_product_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_type ON products USING btree (product_type);


--
-- Name: index_products_on_product_type_and_family; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_type_and_family ON products USING btree (product_type, family);


--
-- Name: index_products_on_product_type_and_family_and_category; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_type_and_family_and_category ON products USING btree (product_type, family, category);


--
-- Name: index_question_categories_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_question_categories_on_url ON question_categories USING btree (url);


--
-- Name: index_question_subcategories_on_published; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_question_subcategories_on_published ON question_subcategories USING btree (published);


--
-- Name: index_question_subcategories_on_question_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_question_subcategories_on_question_category_id ON question_subcategories USING btree (question_category_id);


--
-- Name: index_questions_on_fisher_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_questions_on_fisher_id ON questions USING btree (fisher_id);


--
-- Name: index_questions_on_published; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_questions_on_published ON questions USING btree (published);


--
-- Name: index_questions_on_question_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_questions_on_question_category_id ON questions USING btree (question_subcategory_id);


--
-- Name: index_questions_on_question_subcategory_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_questions_on_question_subcategory_id ON questions USING btree (question_subcategory_id);


--
-- Name: index_stores_on_city; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_city ON stores USING btree (city);


--
-- Name: index_stores_on_hidden; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_hidden ON stores USING btree (hidden);


--
-- Name: index_stores_on_province; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_province ON stores USING btree (province);


--
-- Name: index_stores_on_region; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_region ON stores USING btree (region);


--
-- Name: index_stores_on_region_and_province; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_region_and_province ON stores USING btree (region, province);


--
-- Name: index_stores_on_region_and_province_and_city; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stores_on_region_and_province_and_city ON stores USING btree (region, province, city);


--
-- Name: index_taggings_on_catalogue_item_id_and_tag_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_taggings_on_catalogue_item_id_and_tag_id ON taggings USING btree (catalogue_item_id, tag_id);


--
-- Name: index_tags_on_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_tags_on_url ON tags USING btree (url);


--
-- Name: subcategory_url; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX subcategory_url ON question_subcategories USING btree (url);


--
-- Name: type_family; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX type_family ON catalogue_items USING btree (product_family_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20120202173340');

INSERT INTO schema_migrations (version) VALUES ('20120202183227');

INSERT INTO schema_migrations (version) VALUES ('20120204161948');

INSERT INTO schema_migrations (version) VALUES ('20120206150114');

INSERT INTO schema_migrations (version) VALUES ('20120208121613');

INSERT INTO schema_migrations (version) VALUES ('20120208202301');

INSERT INTO schema_migrations (version) VALUES ('20120208210256');

INSERT INTO schema_migrations (version) VALUES ('20120210172130');

INSERT INTO schema_migrations (version) VALUES ('20120212111906');

INSERT INTO schema_migrations (version) VALUES ('20120212140413');

INSERT INTO schema_migrations (version) VALUES ('20120212155759');

INSERT INTO schema_migrations (version) VALUES ('20120215194812');

INSERT INTO schema_migrations (version) VALUES ('20120215201527');

INSERT INTO schema_migrations (version) VALUES ('20120215202617');

INSERT INTO schema_migrations (version) VALUES ('20120215204021');

INSERT INTO schema_migrations (version) VALUES ('20120305130704');

INSERT INTO schema_migrations (version) VALUES ('20120307181223');

INSERT INTO schema_migrations (version) VALUES ('20120319102637');

INSERT INTO schema_migrations (version) VALUES ('20120319104202');

INSERT INTO schema_migrations (version) VALUES ('20120411091843');

INSERT INTO schema_migrations (version) VALUES ('20120411092315');

INSERT INTO schema_migrations (version) VALUES ('20120411144901');

INSERT INTO schema_migrations (version) VALUES ('20120605064433');

INSERT INTO schema_migrations (version) VALUES ('20120605070156');

INSERT INTO schema_migrations (version) VALUES ('20120605194332');

INSERT INTO schema_migrations (version) VALUES ('20120607080806');

INSERT INTO schema_migrations (version) VALUES ('20120608125353');

INSERT INTO schema_migrations (version) VALUES ('20120608125621');

INSERT INTO schema_migrations (version) VALUES ('20120608133656');

INSERT INTO schema_migrations (version) VALUES ('20120608134356');

INSERT INTO schema_migrations (version) VALUES ('20120611202150');

INSERT INTO schema_migrations (version) VALUES ('20120612063613');

INSERT INTO schema_migrations (version) VALUES ('20120612205840');

INSERT INTO schema_migrations (version) VALUES ('20120719180517');

INSERT INTO schema_migrations (version) VALUES ('20120719200528');

INSERT INTO schema_migrations (version) VALUES ('20120719200529');

INSERT INTO schema_migrations (version) VALUES ('20120723150723');

INSERT INTO schema_migrations (version) VALUES ('20120723150834');

INSERT INTO schema_migrations (version) VALUES ('20120723153204');

INSERT INTO schema_migrations (version) VALUES ('20120724202649');

INSERT INTO schema_migrations (version) VALUES ('20120724202821');

INSERT INTO schema_migrations (version) VALUES ('20120726083612');

INSERT INTO schema_migrations (version) VALUES ('20120726101107');

INSERT INTO schema_migrations (version) VALUES ('20120726144030');

INSERT INTO schema_migrations (version) VALUES ('20120726154858');

INSERT INTO schema_migrations (version) VALUES ('20121204161604');

INSERT INTO schema_migrations (version) VALUES ('20121205161802');

INSERT INTO schema_migrations (version) VALUES ('20130609165434');

INSERT INTO schema_migrations (version) VALUES ('20130609174049');

INSERT INTO schema_migrations (version) VALUES ('20130609174207');

INSERT INTO schema_migrations (version) VALUES ('20130609180945');

INSERT INTO schema_migrations (version) VALUES ('20130610140720');

INSERT INTO schema_migrations (version) VALUES ('20130610142754');

INSERT INTO schema_migrations (version) VALUES ('20130613121450');

INSERT INTO schema_migrations (version) VALUES ('20130617093658');

INSERT INTO schema_migrations (version) VALUES ('20130716111703');

INSERT INTO schema_migrations (version) VALUES ('20130716113200');

INSERT INTO schema_migrations (version) VALUES ('20130916155732');

INSERT INTO schema_migrations (version) VALUES ('20131014064000');

INSERT INTO schema_migrations (version) VALUES ('20140303060036');

INSERT INTO schema_migrations (version) VALUES ('20140520140154');

INSERT INTO schema_migrations (version) VALUES ('20140520152902');

INSERT INTO schema_migrations (version) VALUES ('20140526121648');

INSERT INTO schema_migrations (version) VALUES ('20141124164231');

INSERT INTO schema_migrations (version) VALUES ('20141124164445');

INSERT INTO schema_migrations (version) VALUES ('20141124164520');

INSERT INTO schema_migrations (version) VALUES ('20141124164544');

INSERT INTO schema_migrations (version) VALUES ('20141125162756');

INSERT INTO schema_migrations (version) VALUES ('20141126144859');

INSERT INTO schema_migrations (version) VALUES ('20141205103934');

INSERT INTO schema_migrations (version) VALUES ('20150126114148');

INSERT INTO schema_migrations (version) VALUES ('20150615111044');

INSERT INTO schema_migrations (version) VALUES ('20150616125447');

INSERT INTO schema_migrations (version) VALUES ('20150619092108');

INSERT INTO schema_migrations (version) VALUES ('20150619093500');

INSERT INTO schema_migrations (version) VALUES ('20150623102441');

INSERT INTO schema_migrations (version) VALUES ('20150623102536');

INSERT INTO schema_migrations (version) VALUES ('20150623102651');

INSERT INTO schema_migrations (version) VALUES ('20150623103136');

INSERT INTO schema_migrations (version) VALUES ('20150630100408');

INSERT INTO schema_migrations (version) VALUES ('20150630105346');

INSERT INTO schema_migrations (version) VALUES ('20150701084921');

INSERT INTO schema_migrations (version) VALUES ('20150703131223');

INSERT INTO schema_migrations (version) VALUES ('20151210090632');

INSERT INTO schema_migrations (version) VALUES ('20160125135132');

INSERT INTO schema_migrations (version) VALUES ('20160125140043');

INSERT INTO schema_migrations (version) VALUES ('20160404174545');

INSERT INTO schema_migrations (version) VALUES ('20160530094940');

INSERT INTO schema_migrations (version) VALUES ('20160530095354');

INSERT INTO schema_migrations (version) VALUES ('20160530095539');

INSERT INTO schema_migrations (version) VALUES ('20160601120244');