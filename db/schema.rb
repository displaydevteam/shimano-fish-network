# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 0) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "answers", :force => true do |t|
    t.text     "text"
    t.integer  "question_id"
    t.integer  "fisher_id"
    t.boolean  "published"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "likes"
  end

  add_index "answers", ["fisher_id"], :name => "index_answers_on_fisher_id"
  add_index "answers", ["likes"], :name => "index_answers_on_likes"
  add_index "answers", ["published"], :name => "index_answers_on_published"
  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"

  create_table "assistence_requests", :force => true do |t|
    t.integer  "product_registration_id"
    t.date     "started_at"
    t.date     "finished_at"
    t.text     "notes"
    t.datetime "created_at",              :default => '2012-09-20 12:55:50'
    t.datetime "updated_at",              :default => '2012-09-20 12:55:50'
  end

  create_table "attachments", :force => true do |t|
    t.string   "title"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "item_id"
    t.string   "item_type"
    t.string   "link"
  end

  create_table "catalogue_items", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.text     "sizes"
    t.integer  "product_family_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "catalogue_items", ["product_family_id"], :name => "type_family"
  add_index "catalogue_items", ["url"], :name => "index_catalogue_items_on_url"

  create_table "catalogues", :force => true do |t|
    t.string   "title"
    t.string   "image"
    t.integer  "position"
    t.string   "link"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "sku"
    t.string   "file_name"
    t.string   "doc_type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "link"
    t.string   "image"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.boolean  "partecipate"
  end

  create_table "fishers", :force => true do |t|
    t.string   "email",                   :default => "", :null => false
    t.string   "encrypted_password",      :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "name"
    t.string   "surname"
    t.string   "year_of_birth"
    t.string   "region"
    t.string   "job"
    t.boolean  "agonist"
    t.text     "inner_waters_techniques"
    t.text     "sea_techniques"
    t.integer  "products_count",          :default => 0
  end

  add_index "fishers", ["confirmation_token"], :name => "index_fishers_on_confirmation_token", :unique => true
  add_index "fishers", ["email"], :name => "index_fishers_on_email", :unique => true
  add_index "fishers", ["reset_password_token"], :name => "index_fishers_on_reset_password_token", :unique => true

  create_table "galleries", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "homepage_contents", :force => true do |t|
    t.string   "title"
    t.text     "abstract"
    t.string   "link"
    t.string   "image"
    t.boolean  "highlight"
    t.boolean  "published"
    t.boolean  "new_window"
    t.integer  "position",   :default => 0
    t.datetime "created_at", :default => '2012-09-20 12:55:50'
    t.datetime "updated_at", :default => '2012-09-20 12:55:50'
  end

  create_table "photos", :force => true do |t|
    t.integer  "fisher_id"
    t.integer  "gallery_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "published",   :default => false
    t.string   "image"
    t.integer  "likes"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "cover",       :default => false
    t.boolean  "homepage"
  end

  add_index "photos", ["fisher_id"], :name => "index_photos_on_fisher_id"
  add_index "photos", ["gallery_id"], :name => "index_photos_on_gallery_id"
  add_index "photos", ["published"], :name => "index_photos_on_published"

  create_table "post_categories", :force => true do |t|
    t.string   "title"
    t.string   "slug"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "post_taggings", :force => true do |t|
    t.integer  "post_id"
    t.integer  "post_tag_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "post_taggings", ["post_id", "post_tag_id"], :name => "index_post_taggings_on_post_id_and_post_tag_id", :unique => true

  create_table "post_tags", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "count"
  end

  add_index "post_tags", ["url"], :name => "index_post_tags_on_url", :unique => true

  create_table "posts", :force => true do |t|
    t.integer  "post_category_id"
    t.date     "date"
    t.string   "title"
    t.text     "description"
    t.boolean  "published"
    t.integer  "word_id"
    t.string   "image"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "url"
    t.text     "fisher_content"
    t.boolean  "highlight"
  end

  create_table "product_attachments", :force => true do |t|
    t.integer  "catalogue_item_id"
    t.string   "url"
    t.string   "label"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "product_attachments", ["catalogue_item_id"], :name => "index_product_attachments_on_catalogue_item_id"

  create_table "product_families", :force => true do |t|
    t.integer  "product_type_id"
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "product_families", ["product_type_id"], :name => "index_product_families_on_product_type_id"
  add_index "product_families", ["url"], :name => "index_product_families_on_url"

  create_table "product_images", :force => true do |t|
    t.integer  "catalogue_item_id"
    t.string   "image"
    t.integer  "position"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "product_images", ["catalogue_item_id"], :name => "index_product_images_on_catalogue_item_id"

  create_table "product_registrations", :force => true do |t|
    t.integer  "product_id"
    t.integer  "fisher_id"
    t.integer  "store_id"
    t.date     "purchase_date"
    t.date     "warranty_expiration_date"
    t.datetime "created_at",               :default => '2012-09-20 12:55:50'
    t.datetime "updated_at",               :default => '2012-09-20 12:55:50'
  end

  add_index "product_registrations", ["fisher_id"], :name => "index_product_registrations_on_fisher_id"

  create_table "product_types", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "product_types", ["url"], :name => "index_product_types_on_url"

  create_table "products", :force => true do |t|
    t.string   "product_type"
    t.string   "family"
    t.string   "category"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",   :default => '2012-09-20 12:55:50'
    t.datetime "updated_at",   :default => '2012-09-20 12:55:50'
    t.boolean  "hidden",       :default => false
  end

  add_index "products", ["category"], :name => "index_products_on_category"
  add_index "products", ["family"], :name => "index_products_on_family"
  add_index "products", ["hidden"], :name => "index_products_on_hidden"
  add_index "products", ["product_type", "family", "category"], :name => "index_products_on_product_type_and_family_and_category"
  add_index "products", ["product_type", "family"], :name => "index_products_on_product_type_and_family"
  add_index "products", ["product_type"], :name => "index_products_on_product_type"

  create_table "question_categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "position",    :default => 0
    t.string   "group"
  end

  add_index "question_categories", ["url"], :name => "index_question_categories_on_url"

  create_table "question_subcategories", :force => true do |t|
    t.integer  "question_category_id"
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.boolean  "published"
    t.string   "url"
  end

  add_index "question_subcategories", ["published"], :name => "index_question_subcategories_on_published"
  add_index "question_subcategories", ["question_category_id"], :name => "index_question_subcategories_on_question_category_id"
  add_index "question_subcategories", ["url"], :name => "subcategory_url"

  create_table "questions", :force => true do |t|
    t.string   "title"
    t.text     "question"
    t.integer  "fisher_id"
    t.integer  "question_subcategory_id"
    t.boolean  "published",               :default => true
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "position",                :default => 0
  end

  add_index "questions", ["fisher_id"], :name => "index_questions_on_fisher_id"
  add_index "questions", ["published"], :name => "index_questions_on_published"
  add_index "questions", ["question_subcategory_id"], :name => "index_questions_on_question_category_id"
  add_index "questions", ["question_subcategory_id"], :name => "index_questions_on_question_subcategory_id"

  create_table "stores", :force => true do |t|
    t.string   "name"
    t.string   "region"
    t.string   "province"
    t.string   "city"
    t.string   "address"
    t.string   "cap"
    t.string   "phone"
    t.string   "website"
    t.datetime "created_at", :default => '2012-09-20 12:55:50'
    t.datetime "updated_at", :default => '2012-09-20 12:55:50'
    t.boolean  "hidden",     :default => false
  end

  add_index "stores", ["city"], :name => "index_stores_on_city"
  add_index "stores", ["hidden"], :name => "index_stores_on_hidden"
  add_index "stores", ["province"], :name => "index_stores_on_province"
  add_index "stores", ["region", "province", "city"], :name => "index_stores_on_region_and_province_and_city"
  add_index "stores", ["region", "province"], :name => "index_stores_on_region_and_province"
  add_index "stores", ["region"], :name => "index_stores_on_region"

  create_table "taggings", :force => true do |t|
    t.integer "catalogue_item_id"
    t.integer "tag_id"
  end

  add_index "taggings", ["catalogue_item_id", "tag_id"], :name => "index_taggings_on_catalogue_item_id_and_tag_id", :unique => true

  create_table "tags", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tags", ["url"], :name => "index_tags_on_url", :unique => true

  create_table "user_events", :force => true do |t|
    t.integer  "event_id"
    t.integer  "fisher_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "code"
  end

end
