class AddHiddenFieldToProducts < ActiveRecord::Migration
  def change
    add_column :products, :hidden, :boolean, :default => false
    add_index :products, :hidden
  end
end
