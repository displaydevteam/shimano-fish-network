class AddIndexesForQuestionsSearch < ActiveRecord::Migration
  def up
    execute "create index on questions using gin(to_tsvector('italian', title));"
    execute "create index on questions using gin(to_tsvector('italian', question));"
  end

  def down
  end
end
