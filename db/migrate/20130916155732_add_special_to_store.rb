class AddSpecialToStore < ActiveRecord::Migration
  def change
    add_column :stores, :boat, :boolean
    add_column :stores, :surf, :boolean
    add_column :stores, :spinningsw, :boolean
    add_column :stores, :spinningfw, :boolean
    add_column :stores, :bass, :boolean
    add_column :stores, :pesca_al_colpo, :boolean
    add_column :stores, :carpfishing, :boolean
    add_column :stores, :trota_lago, :boolean
  end
end
