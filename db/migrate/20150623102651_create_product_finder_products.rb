class CreateProductFinderProducts < ActiveRecord::Migration
  def change
    create_table :product_finder_products do |t|
      t.integer :product_finder_answer_id
      t.string :title
      t.string :product_type
      t.string :link
      t.string :iamge

      t.timestamps
    end
  end
end
