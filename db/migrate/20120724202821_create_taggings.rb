class CreateTaggings < ActiveRecord::Migration
  def change
    create_table :taggings, :id => false do |t|
      t.integer :catalogue_item_id
      t.integer :tag_id
    end
  end
end
