class AddCodeToUserEvent < ActiveRecord::Migration
  def change
    add_column :user_events, :code, :string
  end
end
