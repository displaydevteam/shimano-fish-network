class CreateCatalogues < ActiveRecord::Migration
  def change
    create_table :catalogues do |t|
      t.string :title
      t.string :image
      t.integer :position
      t.string :link

      t.timestamps
    end
  end
end
