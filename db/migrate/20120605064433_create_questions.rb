class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.text :question
      t.references :fisher
      t.references :question_category
      t.boolean :published, :default => true

      t.timestamps
    end
    add_index :questions, :fisher_id
    add_index :questions, :published
    add_index :questions, :question_category_id
  end
end
