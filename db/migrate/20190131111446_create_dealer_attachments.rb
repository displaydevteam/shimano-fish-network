class CreateDealerAttachments < ActiveRecord::Migration
  def change
    create_table :dealer_attachments do |t|
      t.string :title
      t.string :file
      t.boolean :published

      t.timestamps
    end
  end
end
