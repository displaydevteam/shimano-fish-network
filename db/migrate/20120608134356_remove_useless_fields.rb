class RemoveUselessFields < ActiveRecord::Migration
  def down
    remove_column :question_categories, :group
    remove_column :questions, :position
  end
end
