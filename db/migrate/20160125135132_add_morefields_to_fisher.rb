class AddMorefieldsToFisher < ActiveRecord::Migration
  def change
    add_column :fishers, :birth_date, :date
    add_column :fishers, :mobile_number, :string
    add_column :fishers, :address, :text
  end
end
