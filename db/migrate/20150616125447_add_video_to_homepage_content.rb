class AddVideoToHomepageContent < ActiveRecord::Migration
  def change
    add_column :homepage_contents, :video, :string
  end
end
