class AddAddressToProductRegistration < ActiveRecord::Migration
  def change
    add_column :product_registrations, :address, :text
  end
end
