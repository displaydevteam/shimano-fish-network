class AddAttachmentToContest < ActiveRecord::Migration
  def change
    add_column :contests, :attachment, :string
  end
end
