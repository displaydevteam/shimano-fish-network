class CreateProductFinderProductanswers < ActiveRecord::Migration
  def change
    create_table :product_finder_productanswers do |t|
      t.integer :product_finder_answer_id
      t.integer :product_finder_product_id

      t.timestamps
    end
  end
end
