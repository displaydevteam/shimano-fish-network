class AddLatLngToStore < ActiveRecord::Migration
  def change
    add_column :stores, :geo_data, :text
    add_column :stores, :reserve, :boolean
  end
end
