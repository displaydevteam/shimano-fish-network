class AddFieldsToDealerAssistenceRequest < ActiveRecord::Migration
  def change
    add_column :dealer_assistance_requests, :product_id, :integer
    add_column :dealer_assistance_requests, :in_warranty, :boolean
    add_column :dealer_assistance_requests, :with_receipt, :boolean
    add_column :dealer_assistance_requests, :receipt, :string
    add_column :dealer_assistance_requests, :picture, :string
    add_column :dealer_assistance_requests, :production_code, :string
    add_column :dealer_assistance_requests, :billing_info, :string
    add_column :dealer_assistance_requests, :billing_date, :date
  end
end
