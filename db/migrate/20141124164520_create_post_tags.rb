class CreatePostTags < ActiveRecord::Migration
  def change
    create_table :post_tags do |t|
      t.string :name
      t.string :url

      t.timestamps
    end 
  add_index :post_tags, :url, :unique => true
  end
end
