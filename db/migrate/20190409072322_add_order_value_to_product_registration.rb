class AddOrderValueToProductRegistration < ActiveRecord::Migration
  def change
    add_column :product_registrations, :order_value, :integer
  end
end
