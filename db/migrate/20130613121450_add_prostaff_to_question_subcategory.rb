class AddProstaffToQuestionSubcategory < ActiveRecord::Migration
  def change
    add_column :question_subcategories, :prostaff, :boolean
  end
end
