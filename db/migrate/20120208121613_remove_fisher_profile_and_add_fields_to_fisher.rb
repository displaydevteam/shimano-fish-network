class RemoveFisherProfileAndAddFieldsToFisher < ActiveRecord::Migration
  def up
    remove_index :fisher_profiles, :fisher_id
    drop_table :fisher_profiles
    
    add_column :fishers, :job, :string  
    add_column :fishers, :agonist, :boolean
    add_column :fishers, :inner_waters_techniques, :text
    add_column :fishers, :sea_techniques, :text
    
  end

  def down
    create_table(:fisher_profiles) do |t|
      t.integer :fisher_id
      t.string :job
      t.boolean :agonist
      t.string :phone
      t.text :inner_waters_techniques
      t.text :sea_techniques
    end
    add_index :fisher_profiles, :fisher_id, :unique => true
  end
end