class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.string :url

      t.timestamps
    end
    add_index :tags, :url, :unique => true
  end
end
