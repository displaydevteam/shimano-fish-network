class CreateQuestionCategories < ActiveRecord::Migration
  def change
    create_table :question_categories do |t|
      t.string :name
      t.text :description
      t.string :url

      t.timestamps
    end
    add_index :question_categories, :url
  end
end
