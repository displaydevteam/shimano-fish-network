class AddFieldsToFisher < ActiveRecord::Migration
  def change
    add_column :fishers, :name, :string
    add_column :fishers, :surname, :string
    add_column :fishers, :year_of_birth, :string
    add_column :fishers, :city, :string
  end
end
