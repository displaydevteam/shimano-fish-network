class CreateAssistenceRequests < ActiveRecord::Migration
  def change
    create_table(:assistence_requests) do |t|
      t.integer :product_registration_id
      t.date :started_at
      t.date :finished_at
      t.text :notes
    end
  end
end
