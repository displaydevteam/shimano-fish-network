class AddUrlToQuestionSubcategories < ActiveRecord::Migration

  def up
    add_column :question_subcategories, :url, :string
    add_index :question_subcategories, :url, :name => :subcategory_url
  end

  def down
    remove_column :question_subcategories, :url, :name => :subcategory_url
  end

end
