class CreateProductAttachments < ActiveRecord::Migration
  def change
    create_table :product_attachments do |t|
      t.integer :catalogue_item_id
      t.string :url
      t.string :label

      t.timestamps
    end
    add_index :product_attachments, :catalogue_item_id
  end
end
