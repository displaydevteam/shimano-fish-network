class AddLinkToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :link, :string
  end
end
