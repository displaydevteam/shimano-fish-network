class CerateProductRegistrations < ActiveRecord::Migration
  def change
    create_table(:product_registrations) do |t|
      t.integer :product_id
      t.integer :fisher_id
      t.integer :store_id
      t.date :purchase_date
      t.date :warranty_expiration_date
    end
  end
end
