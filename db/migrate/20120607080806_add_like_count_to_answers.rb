class AddLikeCountToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :likes, :integer
    add_index :answers, :likes
  end
end
