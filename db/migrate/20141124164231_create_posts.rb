class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :post_category_id
      t.date :date
      t.string :title
      t.text :description
      t.boolean :published
      t.integer :word_id
      t.string :image

      t.timestamps
    end
  end
end
