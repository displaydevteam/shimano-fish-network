class AddStatusToDealerAssistanceRequest < ActiveRecord::Migration
  def change
    add_column :dealer_assistance_requests, :status, :string
  end
end
