class RenameCityColumnToRegionForFishers < ActiveRecord::Migration
  def up
    rename_column :fishers, :city, :region
  end

  def down
    rename_column :fishers, :region, :city
  end
end