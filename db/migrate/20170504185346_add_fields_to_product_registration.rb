class AddFieldsToProductRegistration < ActiveRecord::Migration
  def change
    add_column :product_registrations, :product_photo, :string
    add_column :product_registrations, :receipt_photo, :string
  end
end
