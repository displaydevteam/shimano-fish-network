class AddImageToProductFinderAnswer < ActiveRecord::Migration
  def change
    add_column :product_finder_answers, :image, :string
  end
end
