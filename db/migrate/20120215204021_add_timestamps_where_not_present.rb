class AddTimestampsWhereNotPresent < ActiveRecord::Migration
  def change
    add_column :homepage_contents, :created_at, :datetime, :default => Time.now
    add_column :homepage_contents, :updated_at, :datetime, :default => Time.now
    
    add_column :product_registrations, :created_at, :datetime, :default => Time.now
    add_column :product_registrations, :updated_at, :datetime, :default => Time.now
    
    add_column :assistence_requests, :created_at, :datetime, :default => Time.now
    add_column :assistence_requests, :updated_at, :datetime, :default => Time.now
    
    add_column :stores, :created_at, :datetime, :default => Time.now
    add_column :stores, :updated_at, :datetime, :default => Time.now
    
    add_column :products, :created_at, :datetime, :default => Time.now
    add_column :products, :updated_at, :datetime, :default => Time.now
    
  end
end
