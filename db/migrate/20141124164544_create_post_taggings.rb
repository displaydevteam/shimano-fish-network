class CreatePostTaggings < ActiveRecord::Migration
  def change
    create_table :post_taggings do |t|
      t.integer :post_id
      t.integer :post_tag_id

      t.timestamps
    end
    add_index :post_taggings, [:post_id, :post_tag_id], :unique => true
  end
end
