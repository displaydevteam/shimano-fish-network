class AddGreenToStore < ActiveRecord::Migration
  def change
    add_column :stores, :green, :boolean
  end
end
