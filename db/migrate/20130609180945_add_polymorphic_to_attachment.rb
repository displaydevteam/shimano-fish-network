class AddPolymorphicToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :item_id, :integer
    add_column :attachments, :item_type, :string
  end
end
