class CreateHomepageContents < ActiveRecord::Migration
  def change
    create_table(:homepage_contents) do |t|
      t.string  :title
      t.text    :abstract
      t.string  :link
      t.string  :image
      t.boolean :highlight
    end
  end
end
