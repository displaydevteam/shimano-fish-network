class AddDescriptionToDealerAssistanceRequest < ActiveRecord::Migration
  def change
    add_column :dealer_assistance_requests, :description, :text
  end
end
