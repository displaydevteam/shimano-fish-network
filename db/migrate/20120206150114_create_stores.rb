class CreateStores < ActiveRecord::Migration
  def change
    create_table(:stores) do |t|
      t.string :name
      t.string :region
      t.string :province
      t.string :city
      t.string :address
      t.string :cap
      t.string :phone
      t.string :website
    end
    add_index :stores, :region
    add_index :stores, :province
    add_index :stores, :city
    add_index :stores, [:region, :province]
    add_index :stores, [:region, :province, :city]
  end
end
