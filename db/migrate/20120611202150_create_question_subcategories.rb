class CreateQuestionSubcategories < ActiveRecord::Migration
  def change
    create_table :question_subcategories do |t|
      t.references :question_category
      t.string :name

      t.timestamps
    end
    add_index :question_subcategories, :question_category_id
    rename_column :questions, :question_category_id, :question_subcategory_id
    add_index :questions, :question_subcategory_id
  end
end
