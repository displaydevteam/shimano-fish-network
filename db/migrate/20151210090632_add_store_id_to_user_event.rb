class AddStoreIdToUserEvent < ActiveRecord::Migration
  def change
    add_column :user_events, :store_id, :integer
  end
end
