class CreateProductFinderAnswers < ActiveRecord::Migration
  def change
    create_table :product_finder_answers do |t|
      t.integer :product_finder_question_id
      t.string :title
      t.string :url
      t.integer :next_question_id

      t.timestamps
    end
  end
end
