class AddCampaignsToFisher < ActiveRecord::Migration
  def change
    add_column :fishers, :campaign_1, :boolean
    add_column :fishers, :campaign_2, :boolean
    add_column :fishers, :campaign_3, :boolean
    add_column :fishers, :campaign_4, :boolean
  end
end
