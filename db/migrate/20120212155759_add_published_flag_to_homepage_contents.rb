class AddPublishedFlagToHomepageContents < ActiveRecord::Migration
  def change
    add_column :homepage_contents, :published, :boolean
  end
end
