class CreateCatalogueItems < ActiveRecord::Migration
  def change
    create_table :catalogue_items do |t|
      t.string :name
      t.string :url
      t.text :description
      t.text :sizes
      t.integer :product_family_id
      t.timestamps
    end
    add_index :catalogue_items, [:product_family_id], :name => 'type_family'
    add_index :catalogue_items, :url
  end
end
