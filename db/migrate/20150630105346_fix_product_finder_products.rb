class FixProductFinderProducts < ActiveRecord::Migration
  def up
    rename_column :product_finder_products, :iamge, :image
    rename_column :product_finder_products, :title, :fishing_reel_name
    rename_column :product_finder_products, :link, :fishing_reel_link
    add_column :product_finder_products, :fishing_rod_name, :string
    add_column :product_finder_products, :fishing_rod_link, :string
    add_column :product_finder_products, :fishing_reel_image, :string
    add_column :product_finder_products, :fishing_rod_image, :string
  end

  def down
    rename_column :product_finder_products, :image, :iamge
    rename_column :product_finder_products, :fishing_reel_name, :title
    rename_column :product_finder_products, :fishing_reel_link, :link
    remove_column :product_finder_products, :fishing_rod_name
    remove_column :product_finder_products, :fishing_rod_link
    remove_column :product_finder_products, :fishing_reel_image
    remove_column :product_finder_products, :fishing_rod_image
  end
end
