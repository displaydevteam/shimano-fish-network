class AddHiddenFieldToStores < ActiveRecord::Migration
  def change
    add_column :stores, :hidden, :boolean, :default => false
    add_index :stores, :hidden
  end
end
