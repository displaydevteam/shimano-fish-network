class ProductTypes < ActiveRecord::Migration
  def change
    create_table :product_types do |t|
      t.string :name
      t.string :url
      t.text :description
      t.timestamps
    end
    add_index :product_types, :url
  end
end
