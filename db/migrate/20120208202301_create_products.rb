class CreateProducts < ActiveRecord::Migration
  def change
    create_table(:products) do |t|
      t.string :product_type
      t.string :family
      t.string :category
      t.string :code
      t.string :description
    end
    add_index :products, :product_type
    add_index :products, :family
    add_index :products, :category
    add_index :products, [:product_type, :family]
    add_index :products, [:product_type, :family, :category]
  end
end
