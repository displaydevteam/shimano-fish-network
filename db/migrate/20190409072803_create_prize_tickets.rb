class CreatePrizeTickets < ActiveRecord::Migration
  def change
    create_table :prize_tickets do |t|
      t.string :email
      t.string :code

      t.timestamps
    end
  end
end
