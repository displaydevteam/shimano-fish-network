class CreateDealerAssistanceRequests < ActiveRecord::Migration
  def change
    create_table :dealer_assistance_requests do |t|
      t.string :sku
      t.integer :dealer_id
      t.date :date

      t.timestamps
    end
  end
end
