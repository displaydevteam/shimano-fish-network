class CreateShimanoContestUsers < ActiveRecord::Migration
  def change
    create_table :shimano_contest_users do |t|
      t.string :email
      t.text :form_data

      t.timestamps
    end
  end
end
