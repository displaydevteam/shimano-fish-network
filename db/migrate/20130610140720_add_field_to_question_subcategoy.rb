class AddFieldToQuestionSubcategoy < ActiveRecord::Migration
  def change
    add_column :question_subcategories, :image, :string
    add_column :question_subcategories, :bio, :text
    add_column :question_subcategories, :email, :string
    add_column :question_subcategories, :profile, :string
  end
end
