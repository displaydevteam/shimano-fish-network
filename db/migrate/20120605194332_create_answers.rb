class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.text :text
      t.references :question
      t.references :fisher
      t.boolean :published

      t.timestamps
    end
    add_index :answers, :question_id
    add_index :answers, :fisher_id
    add_index :answers, :published
  end
end
