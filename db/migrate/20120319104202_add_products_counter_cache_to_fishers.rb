class AddProductsCounterCacheToFishers < ActiveRecord::Migration
  def self.up
    add_column :fishers, :products_count, :integer, :default => 0

    Fisher.reset_column_information
    Fisher.all.each do |p|
      p.update_attribute :products_count, p.products.length
    end
  end

  def self.down
    remove_column :fishers, :products_count 
  end
end
