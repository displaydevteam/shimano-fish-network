class AddOptionalTargetBlankToHomepageContents < ActiveRecord::Migration
  def change
    add_column :homepage_contents, :new_window, :boolean
  end
end
