class ProductFamilies < ActiveRecord::Migration
  def change
    create_table :product_families do |t|
      t.integer :product_type_id
      t.string :name
      t.string :url
      t.text :description
      t.timestamps
    end
    add_index :product_families, :product_type_id
    add_index :product_families, :url
  end
end
