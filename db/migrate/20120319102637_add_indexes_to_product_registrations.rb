class AddIndexesToProductRegistrations < ActiveRecord::Migration
  def change
    add_index :product_registrations, :fisher_id
  end
end
