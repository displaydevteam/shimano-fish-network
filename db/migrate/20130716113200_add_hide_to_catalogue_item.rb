class AddHideToCatalogueItem < ActiveRecord::Migration
  def change
    add_column :catalogue_items, :hide, :boolean
  end
end
