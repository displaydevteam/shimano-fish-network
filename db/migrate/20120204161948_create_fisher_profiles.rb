class CreateFisherProfiles < ActiveRecord::Migration
  def change
    create_table(:fisher_profiles) do |t|
      t.integer :fisher_id
      t.string :job
      t.boolean :agonist
      t.string :phone
      t.text :inner_waters_techniques
      t.text :sea_techniques
    end
    add_index :fisher_profiles, :fisher_id, :unique => true
  end
end