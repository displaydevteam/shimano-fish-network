class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images do |t|
      t.integer :catalogue_item_id
      t.string :image
      t.integer :position

      t.timestamps
    end
    add_index :product_images, :catalogue_item_id
  end
end
