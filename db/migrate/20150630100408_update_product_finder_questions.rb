class UpdateProductFinderQuestions < ActiveRecord::Migration
  def up
    remove_column :product_finder_questions, :root
    add_column :product_finder_questions, :product_finder_answer_id, :integer
    add_index :product_finder_questions, :product_finder_answer_id
  end

  def down
    add_column :product_finder_questions, :root, :boolean
    remove_column :product_finder_questions, :product_finder_answer_id
  end
end
