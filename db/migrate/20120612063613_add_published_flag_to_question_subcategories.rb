class AddPublishedFlagToQuestionSubcategories < ActiveRecord::Migration
  def change
    add_column :question_subcategories, :published, :boolean
    add_index :question_subcategories, :published
  end
end
