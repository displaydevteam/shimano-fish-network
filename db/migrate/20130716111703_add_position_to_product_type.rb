class AddPositionToProductType < ActiveRecord::Migration
  def change
    add_column :product_types, :position, :integer
  end
end
