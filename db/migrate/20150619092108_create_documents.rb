class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.string :sku
      t.string :file_name
      t.string :type

      t.timestamps
    end
  end
end
