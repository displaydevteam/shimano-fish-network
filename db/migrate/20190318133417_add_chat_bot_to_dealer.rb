class AddChatBotToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :chatbot, :boolean
  end
end
