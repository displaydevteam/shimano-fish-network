class AddIndexesForProductsSearch < ActiveRecord::Migration
  def up
    execute "create index on catalogue_items using gin(to_tsvector('italian', name));"
    execute "create index on catalogue_items using gin(to_tsvector('italian', description));"
    execute "create index on catalogue_items using gin(to_tsvector('italian', sizes));"
  end

  def down
  end
end
