class AddJapaneseToStore < ActiveRecord::Migration
  def change
    add_column :stores, :jp, :boolean
  end
end
