class CreateDealerAssisteanceRequestProducts < ActiveRecord::Migration
  def change
    create_table :dealer_assisteance_request_products do |t|
    	t.integer :dealer_assistance_request_id
        t.integer :product_id
		t.boolean :warranty
		t.boolean :receipt
		t.date :receipt_date
		t.string :client_full_name
		t.string :client_email
		t.text :problem_description
		t.text :request_part
      t.timestamps
    end
  end
end
