class RemoveTaggings < ActiveRecord::Migration
  def up
    drop_table :taggings
    create_table :catalogue_items_tags, :id => false do |t|
      t.integer :catalogue_item_id
      t.integer :tag_id
    end
    add_index :catalogue_items_tags, [:catalogue_item_id, :tag_id], :unique => true
  end

  def down
    drop_table :catalogue_items_tags
    create_table :taggings, :id => false do |t|
      t.integer :catalogue_item_id
      t.integer :tag_id
    end
  end
end
