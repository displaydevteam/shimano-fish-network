class AddOrderToHomepageContents < ActiveRecord::Migration
  def change
    add_column :homepage_contents, :position, :integer, :default => 0
  end
end
