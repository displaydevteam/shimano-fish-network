class CreateProductFinderQuestions < ActiveRecord::Migration
  def change
    create_table :product_finder_questions do |t|
      t.string :title
      t.string :url
      t.boolean :root

      t.timestamps
    end
  end
end
