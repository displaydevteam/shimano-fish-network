class AddOrderingToProductFinderProduct < ActiveRecord::Migration
  def change
    add_column :product_finder_products, :ordering, :integer
  end
end
