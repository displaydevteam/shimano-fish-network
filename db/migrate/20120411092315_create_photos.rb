class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.references :fisher
      t.references :gallery
      t.string :title
      t.text :description
      t.boolean :published, :default => false
      t.string :image
      t.integer :likes
      t.timestamps
    end
    add_index :photos, :published
    add_index :photos, :gallery_id
    add_index :photos, :fisher_id
  end
end
