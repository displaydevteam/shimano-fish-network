ShimanoFishNetwork::Application.routes.draw do



  get "contests/index"

  devise_for :dealers

  get "post_tags/index"

  mount Ckeditor::Engine => '/ckeditor'


  get "posts/show"

  root :to => 'home#index'
  
  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config


  def get_or_post(path, opts={}, &block)
    get(path, opts, &block)
    post(path, opts, &block)
  end

  get 'download' => 'home#download'
  
  
  namespace "admin" do
    root :to => 'data_manipulations#index'
    resources :data_manipulations, :only => [:index, :show, :create]
  end

  
  get '/dealers' => 'dealer#index'
  get '/dealers/dl' => 'dealer#download', as: :dealer_download
  post '/api/store_contest' => 'home#store_contest'
  get '/import_campaign' => 'home#import'
  get '/clean_p' => 'home#clean_p'
  get 'upload' => 'catalog#index'
  post 'upload' => 'catalog#create', :as => 'data_manipulations'
  get 'products/suggest' => 'products#suggest', :as => 'product_suggest'
  get 'shimano-shop' => 'stores#index', :as => 'store_locator'
  get 'shimano-shop/suggest' => 'stores#suggest', :as => 'store_suggest'
  get 'shimano-shop/regions' => 'stores#regions', :as => 'store_locator_regions'
  get 'shimano-shop/provinces' => 'stores#provinces', :as => 'store_locator_provinces'
  get 'shimano-shop/cities' => 'stores#cities', :as => 'store_locator_cities'
  get 'shimano-shop/:specialist' => 'stores#specialist'
  get 'cataloghi-shimano' => 'press#index'
  get 'concorso' => 'contests#home'
  get 'concorso/gioca' => 'contests#play'
  get 'concorso/gioca/risultato' => 'contests#result'
  get 'concorso/premi' => 'contests#prizes'
  get '/product_list' => 'products#list'
  get '/products_dealers' => 'products#dealer'
  get '/green-line'      => 'contests#index', as: :contest
  get '/green-line/:id'      => 'contests#vote'
  get '/blog' => 'posts#index'
  scope '/blog' do 
    get '/category/:slug' => 'posts#category'
    get '/tag/:url' => 'posts#tag'
    get '/:date/:title' => 'posts#show', :as => :post_show
    get '/:year/:day/:month/:title' => 'posts#show'
  end
  scope '/community' do
    get '/' => 'communities#index', :as => :community
    get '/riserva' => 'events#reserve', :as => :reserve
    scope '/eventi' do
      get '/' => 'events#index', :as => :events
      get '/archive' => 'events#archive', :as => :events_archive
      get '/(:title)--(:id)' => 'events#show', :as => :event
      get '/(:title)--(:id)/list' => 'events#list', :as => :event_list
      get '/(:title)--(:id)/attend' => 'events#attend', :as => :attend_event
    end
    scope '/galleria' do
      get '/' => 'galleries#index', :as => :galleries
      get '/:id' => 'galleries#show', :as => :gallery
      get '/:gallery_id/foto/:id' => 'photos#show', :as => :photo
      post '/:gallery_id/photos/:id' => 'photos#like', :as => :like_photo
    end
    scope '/galleries' do
      match "/", to: redirect('/galleria')
      match "/:id", to: redirect('/%{id}')
      match '/:gallery_id/photo/:id', to: redirect('/%{gallery_id}/foto/%{id}')
    end
  end

  match '/community/ask-shimano', to: redirect('/ask-shimano'), :status => 301
  match '/community/ask-shimano/*params', to: redirect {|params| "/ask-shimano/#{CGI::unescape(params[:params])}" }, :status => 301

  scope '/garanzia/ask-shimano' do

   
      get '/' => 'question_categories#index', :as => :ask_shimano
      get '/mondo-artificiali/(*params)', to: redirect {|params| "/ask-shimano/tecniche-e-attrezzatura/#{CGI::unescape(params[:params])}" }, :status => 301
      get '/attrezzatura/(*params)', to: redirect {|params| "/ask-shimano/tecniche-e-attrezzatura/#{CGI::unescape(params[:params])}" }, :status => 301
      get '/tecniche-di-pesca/(*params)', to: redirect {|params| "/ask-shimano/tecniche-e-attrezzatura/#{CGI::unescape(params[:params])}" }, :status => 301
      get '/search' => 'questions#search', :as => :search_questions_path
      get '/:category_url(/:subcategory_url)' => 'questions#index', :as => :questions
      get '/:category_url/new-question/expert' => 'questions#expert', :as => :new_expert
      get '/:category_url(/:subcategory_url/)new-question/' => 'questions#new', :as => :new_question
      post '/:category_url' => 'questions#create'
      get '/:category_url/:subcategory_url/:id' => 'questions#show', :as => :question
      post '/:category_url/:subcategory_url/:id/like-answer/:answer_id' => 'questions#like_answer', :as => :like_question_answer
      post '/:category_url/:subcategory_url/:id/send-answer' => 'questions#create_answer', :as => :question_answer
  end

  

  match "/catalogo", to: redirect('/attrezzatura-pesca')
  match "/cataloghi", to: redirect('/cataloghi-shimano')

  scope '/catalogo' do
    match "/", to: redirect('/attrezzatura-pesca')
    match '/:product_type/', to: redirect('/attrezzatura-pesca/%{product_type}')
    match '/:product_type/:product_family', to: redirect('/attrezzatura-pesca/%{product_type}/%{product_family}/')
    match '/:product_type/:product_family/:product_url', to: redirect('/attrezzatura-pesca/%{product_type}--%{product_url}/')
    get '/:product_type/:product_family/tags/(*tags)', to: redirect("/attrezzatura-pesca/%{product_type}/%{product_family}/tags/%{tags}")
  end


  get '/dealers' => 'dealers#index'
  get '/dealers/new' => 'dealers#new'
  get '/dealers/:id' => 'dealers#show'
  post '/dealers/new' => 'dealers#create', as: :dealer_assistance_requests

  match "/attrezzatura-pesca" => 'product_finder#index'

  scope '/attrezzatura-pesca' do
    get '/trova/' => 'product_finder#index'
    get '/trova/*slug' => 'product_finder#index'
    get '/*das', to: redirect('/attrezzatura-pesca')
  end

  get '/tags' => 'tags#index'
  get '/posttags' => 'post_tags#index'

  scope "/garanzia"  do

    devise_for :fishers, :path => '', :skip => [:registrations], :controllers => { :sessions => 'fishing_club/sessions', :confirmations => 'fishing_club/confirmations', :passwords => 'fishing_club/passwords'}
    devise_scope :fisher do
    get 'pro-staff' => 'prostaff#index', :as =>   'pro_staff'
    get 'pro-staff/:url' => 'prostaff#show', :as => 'show_pro_staff'
    get 'prodotti' => 'fishing_club/fishing_club#product'
    get 'eventi' => 'fishing_club/fishing_club#events'
    get "modifica-dati-personali" => "fishing_club/registrations#edit", :as => 'edit_fisher_registration'
      get "cancella" => "fishing_club/registrations#cancel", :as => 'cancel_fisher_registration'
      delete "cancella-iscrizione" => 'fishing_club/registrations#destroy', :as => 'destroy_fisher_registration'
      post "/" => "fishing_club/registrations#create", :as => 'fisher_registration'
      put "/" => "fishing_club/registrations#update"
      post "/" => "fishing_club/registrations#destroy"
      get "registrati" => "fishing_club/registrations#new", :as => 'new_fisher_registration'
    end
    resources :photos, :controller => 'fishing_club/photos', :only => [:new, :create]
    resources :product_registrations, :controller => 'fishing_club/product_registrations', :only => [:index, :new, :create, :show, :update, :edit] do
      collection do
        post 'confirm'
      end
    end
    get '/' => 'fishing_club/fishing_club#index', :as => 'fishing_club'
  end

  get '/:page' => 'pages#show'
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
