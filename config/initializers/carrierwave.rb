module CarrierWave
  module Storage
    class Fog < Abstract
      class File

        def public_url
          if host = @uploader.asset_host
            "#{host}/#{path}"
          else
            case @uploader.fog_credentials[:provider]
            when 'AWS'
              "https://#{@uploader.fog_directory}.s3.amazonaws.com/#{path}"
            when 'Google'
              "https://commondatastorage.googleapis.com/#{@uploader.fog_directory}/#{path}"
            else
              # avoid a get by just using local reference
              directory.files.new(:key => path).public_url
            end
          end
        end
      end
    end
  end
  module CarrierWaveStoreDirConfig
    def store_dir
      if Rails.env.test? or Rails.env.cucumber? 
        Rails.root.join('tmp', 'uploads')
      else
        "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end
end


unless Rails.env.test? or Rails.env.cucumber?
  CarrierWave.configure do |config|
    config.cache_dir = "#{::Rails.root.to_s}/tmp/uploads"
    config.storage = :fog
    config.fog_credentials = {
      :provider               => 'AWS',       # required
      :aws_access_key_id      => 'AKIA2SUROVWBALMJXYEC',       # required
      :aws_secret_access_key  => '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',       # required
      :region                 => 'eu-central-1'  # optional, defaults to 'us-east-1'
    }
    config.fog_directory  = 'shimano-production'                     # required
    # config.fog_host       = 'https://assets.example.com'            # optional, defaults to nil
    # config.fog_public     = false                                   # optional, defaults to true
    # config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}

  end
else
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = !(Rails.env.test? or Rails.env.cucumber?)
  end
end