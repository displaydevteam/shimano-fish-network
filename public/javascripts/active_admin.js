/* Active Admin JS */
$(function(){
  $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});

  $(".clear_filters_btn").click(function(){
    window.location.search = "";
    return false;
  });

  $('#catalogue_item_tag_tokens').tokenInput('/tags.json', {
    theme : 'facebook',
    prePopulate: $('#catalogue_item_tag_tokens').data('load')
  });

  $('form').on('click', '.remove_item', function(event){
    $(this).parent().prev().find('input[type=hidden]').val('1');
    $(this).closest('fieldset').hide()
    event.preventDefault();
  });

  $('form').on('click', '.add_fields', function(event){
    time = new Date().getTime();
    regexp = new RegExp($(this).data('id'), 'g');
    $(this).before($(this).data('fields').replace(regexp, time));
    event.preventDefault();
  });

});
