$(document).ready(function(){
    //Init jQuery Masonry layout
    init_masonry();

});

function init_masonry(){
    var $container = $('#blg');

    $container.imagesLoaded( function(){
        $container.masonry({
            itemSelector : '.item',
            gutterWidth: 0,
            isAnimated: true,
            columnWidth: function( containerWidth ) {
              return containerWidth / 2;
            }
        });
    });
}

